#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpkg/architecture.mk

#export DEB_BUILD_MAINT_OPTIONS = hardening=+all
#export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

l_SONAME=63

TARGETS:= native32 native64
CONFIGURE_FLAGS+= --enable-static --disable-layoutex --disable-icu-config

export DEB_BUILD_MAINT_OPTIONS=dilos=-saveargs,-ld_b_direct

DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)

SAVE_ARGS=
ifeq ($(DEB_HOST_ARCH),solaris-i386)
usr_lib32 = /usr/lib
endif

ifeq ($(DEB_HOST_ARCH),solaris-sparc)
usr_lib32 = /usr/lib
endif

override_dh_auto_clean:
	rm -f doconfigure-native* dobuild-native*
	dh_auto_clean --no-parallel

override_dh_clean:
	dh_clean
	find $(CURDIR)/source/ \( -name Makefile -o -name pkgdataMakefile \) \
		-exec rm {} \;
	rm -Rf build-native64 build-native32

override_dh_auto_configure: $(TARGETS:%=doconfigure-%)

doconfigure-%:
ifeq ($(DEB_BUILD_ARCH),$(DEB_HOST_ARCH))
	dh_auto_configure -B $(CURDIR)/build-$(*) -- $(CONFIGURE_FLAGS) 
else
	dh_auto_configure -B $(CURDIR)/build-$(*) -- --host=$(DEB_BUILD_GNU_TYPE) --disable-layoutex --disable-icu-config
	dh_auto_build -B $(CURDIR)/build-$(*)
	dh_auto_configure -- --enable-static --with-cross-build=$(CURDIR)/build-$(*)
endif
	touch $@

doconfigure-native32: CONFIGURE_FLAGS+= CXXFLAGS="$(CXXFLAGS) -m32" CFLAGS="$(CFLAGS) -m32" --libdir=$(usr_lib32) --libexecdir=$(usr_lib32)
doconfigure-native64: CONFIGURE_FLAGS+= CXXFLAGS="$(CXXFLAGS) $(SAVE_ARGS)" CFLAGS="$(CFLAGS) $(SAVE_ARGS)"

override_dh_auto_build: $(TARGETS:%=dobuild-%)
#override_dh_auto_build-indep: dobuild-native64

dobuild-native64: doconfigure-native64
	dh_auto_build -B $(CURDIR)/build-native64 --parallel -- VERBOSE=1
	dh_auto_build -B $(CURDIR)/build-native64 --parallel -- VERBOSE=1 doc
	touch $@

dobuild-native32: doconfigure-native32
	dh_auto_build -B $(CURDIR)/build-native32 --parallel -- VERBOSE=1
	touch $@


override_dh_auto_test:
ifeq (,$(findstring $(DEB_BUILD_ARCH),i386 hurd-i386 kfreebsd-i386))
	dh_auto_test
else
	# French locale has test problems on x86 architectures
	-dh_auto_test
endif

override_dh_auto_install: $(TARGETS:%=doinstall-%)
	$(MAKE) -C $(CURDIR)/build-native64 install-doc DESTDIR=$(CURDIR)/debian/tmp/
	# delete extra license file
	$(RM) $(CURDIR)/debian/tmp/usr/share/icu/$(l_SONAME).?/LICENSE
	# remove not needed manpage
#	$(RM) $(CURDIR)/debian/tmp/usr/share/man/man1/icu-config.1

doinstall-native32: dobuild-native32
	dh_auto_install -B $(CURDIR)/build-native32

doinstall-native64: dobuild-native64
	dh_auto_install -B $(CURDIR)/build-native64

override_dh_installdocs-indep:
	dh_installdocs -i
	# symlink duplicated files
	for file in `find debian/icu-doc/usr/share/doc/icu-doc/html/ -name \*icu_1_1\*`; do \
		normal=`echo $$file | sed s/icu_1_1//`; \
		if [ -f $$normal ]; then \
			$(RM) $$file; \
			ln -s `basename $$normal` $$file; \
		fi; \
	done

override_dh_missing:
	dh_missing --list-missing

%:
	dh $@ --sourcedirectory=$(CURDIR)/source/

override_dh_strip:
	dh_strip -Xicuinfo -Xlibicutest.so.63.1 -Xgennorm2 -Xescapesrc \
		-Xpkgdata -Xgenrb -Xgendict -Xgenbrk -Xgencnval -Xuconv \
		-Xlibicutest.so.63.1 -Xgencfu -Xmakeconv -Xderb \
		-Xlibicui18n.so.63.1 -Xlibicudata.so.63.1 -Xgenccode \
		-Xgensprep -Xlibicuio.so.63.1 -Xicupkg -Xlibicutu.so.63.1 \
		-Xgencmn -Xlibicuuc.so.63.1

.PHONY: override_dh_auto_clean override_dh_clean override_dh_auto_configure \
	override_dh_auto_build override_dh_auto_test \
	override_dh_auto_install override_dh_installdocs-indep \
	override_dh_missing
