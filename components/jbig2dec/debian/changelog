jbig2dec (0.16-1+dilos1) unstable; urgency=high

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Fri, 05 Jun 2020 15:48:07 +0300

jbig2dec (0.16-1) unstable; urgency=high

  [ upstream ]
  * New bugfix release.
    (no code changes since 0.15+20190209-1)

  [ Jonas Smedegaard ]
  * Set urgency=high, due to ghostscript 9.27 CVE fixes.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 07 Apr 2019 17:52:08 +0200

jbig2dec (0.15+20190209-1) unstable; urgency=medium

  [ upstream ]
  * Development snapshot.
    + Update source/header file copyright notice to 2019
    + Indicate error upon error, do not just warn.
    + Use common pattern for error detection.
    + Clarify the element size of GR_stats when memsetting.
    + Make sure to indicate error upon unexpected OOB.
    + Without enough prefix codes, a Huffman table cannot be generated.
    + Return allocator once it is retired.

  [ Jonas Smedegaard ]
  * Drop patches cherry-picked upstream since applied.
  * Update copyright info: Extend coverage for main upstream author.
  * Update symbols file: Bump version for symbol jbig2_ctx_free.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 04 Apr 2019 12:41:06 +0200

jbig2dec (0.15-2) unstable; urgency=medium

  * Add patches cherry-picked upstream:
    + Fix signed/unsigned mismatches.
    + Only print repeated error/warning messages once.
    + Bring jbig2dec's copy of memento up to date.
  * Simplify rules: Stop resolve build-dependencies in rules file.
  * Stop build-depend on dh-buildinfo.
  * Wrap and sort control file.
  * Update copyright info: Extend coverage for main upstream author.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 21 Nov 2018 01:50:52 +0100

jbig2dec (0.15-1) unstable; urgency=medium

  [ upstream ]
  * New bugfix release.
    (no code changes since 0.14+20180826-1)

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 14 Sep 2018 14:10:04 +0200

jbig2dec (0.14+20180826-1) unstable; urgency=medium

  [ upstream ]
  * Development snapshot.

  [ Jonas Smedegaard ]
  * Unfuzz patches.
  * Declare compliance with Debian Policy 4.2.1.
  * Set Rules-Requires-Root: no.
  * Relax symbols check when targeting experimental
  * Update symbols:
    + Flag as optional symbols not declared in public header file.
    + Drop 10 private symbols.
    + Drop 1 symbol (unused in Debian).
    + Add 1 symbol.
    Update copyright info:
    + Extend coverage for main author to include current year.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 31 Aug 2018 00:12:20 +0200

jbig2dec (0.14-1) unstable; urgency=medium

  [ upstream ]
  * New bugfix release.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Extend copyright of packaging to cover past year.
    + Strip nedless copyright signs.
  * Drop patches cherry-picked upstream since applied.
  * Declare compliance with Debian Policy 4.1.4.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 24 May 2018 14:41:41 +0200

jbig2dec (0.13-6) unstable; urgency=medium

  * Team upload
  * Update Vcs-* fields for the move to salsa.d.o

 -- Didier Raboud <odyx@debian.org>  Sat, 10 Feb 2018 16:29:26 +0100

jbig2dec (0.13-5) unstable; urgency=medium

  * Add DEP-3 header to patch 1001.
  * Advertise DEP-3 format in patch headers.
  * Add patches cherry-picked upstream:
    + Fix decoder error on JBIG2 compressed image.
    + Tidy up unused code.
    + Add sanity check on image sizes.
    + refine test for "Denial of Service" images
    + Prevent SEGV due to integer overflow.
    + Prevent integer overflow vulnerability.
    + Bounds check before reading from image source data.
    + Plug leak of parameter info in command-line tool.
    + Fix memory leak in case of error.
    + Make clipping in image compositing handle underflow.
    + Fix double free in error case.
    + Do bounds checking of read data.
    + Do not grow page if page height is known.
    + Fix SEGV due to error code being ignored.
      Closes: Bug#863279; CVE-2017-9216. Thanks to Salvatore Bonaccorso.
    + Allow for symbol dictionary with 0 symbols.
  * Update watch file: Use substitution strings.
  * Stop put aside auto-generated header file during build: No longer
    shipped upstream.
  * Modernize cdbs:
    + Do copyright-check in maintainer script (not during build).
    + Relax to build-depend unversioned on cdbs.
    + Stop build-depend on licensecheck.
  * Declare compliance with Debian Policy 4.1.0.
  * Update copyright info:
    + Use https protocol in file format URL.
    + Fix rename License section AGPL-3 → AGPL-3+.
  * Tighten lintian overrides regarding License-Reference.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 23 Sep 2017 13:27:40 +0200

jbig2dec (0.13-4) unstable; urgency=medium

  * Add patches cherry-picked upstream to squash signed/unsigned
    warnings and to fix warning for always-false unsigned < 0 tests.
    Closes: Bug#850497. Thanks to Salvatore Bonaccorso.
  * Modernize Vcs-Browser field: Use git subdir (not cgit).
  * Stop override lintian for
    package-needs-versioned-debhelper-build-depends: Fixed in lintian.
  * Update copyright info: Extend coverage of Debian packaging.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 23 Jan 2017 21:13:34 +0100

jbig2dec (0.13-3) unstable; urgency=medium

  * Add patch cherry-picked upstream to prevent checking too early for
    buffer overrun.
  * Modernize CDBS: Build-depend on licensecheck (not devscripts).

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 23 Aug 2016 10:13:47 +0200

jbig2dec (0.13-2) unstable; urgency=medium

  * Fix mark libjbig2dec0 as multi-ach: same.
    Closes: Bug#799916. Thanks to Jacek Szafarkiewicz and Yuriy M.
    Kaminskiy.
  * Add patch 2001 to avoid compile unrelated and unusable Memento
    memory debugging code.
    Closes: Bug#824483. Thanks to Yuriy M. Kaminskiy.
  * Drop symbols for dropped Memento code.
    Thanks to Yuriy M. Kaminskiy.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 16 May 2016 18:35:14 +0200

jbig2dec (0.13-1) unstable; urgency=medium

  [ upstream ]
  * New bugfix release.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Bump file format to version 4.
    + Mangle scanned page to get tarball URLs from tags, and adapt URL
      pattern.
    + Mangle download filename.
    + Mention gbp in usage comment.
  * Use https protocol in Vcs-Git URL.
  * Declare compliance with Debian Policy 3.9.8.
  * Update copyright info:
    + Extend coverage for main author to include recent years.
    + Extend copyright of packaging to cover current year.
  * Update git-buildpackage config: Filter any .gitignore file.
  * Drop patch 2001: Applied upstream.
  * Drop 3 symbols (unused, according to http://codesearch.debian.net/).
  * Fix remove old lintian overrides file.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 10 May 2016 16:51:55 +0200

jbig2dec (0.12+20150918-1) unstable; urgency=medium

  [ upstream ]
  * Snapshot.
    + Tidy build configuration.
    + Update for modern libpng.
    + Commit of build_consolidation branch.
    + Fixes for Windows build with VS 2015.
    + Check that cloned image exists before proceeding further.
    + Release huffman table memory properly.

  [ Jonas Smedegaard ]
  * Fix lintian overrides.
  * Unfuzz all patches.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 26 Sep 2015 17:33:05 +0200

jbig2dec (0.12-2) unstable; urgency=medium

  * Move package maintenance to printing team.
  * Suppress lintian warning about build-depending unversioned on
    debhelper.
  * Update copyright info: Fix strip stray License field.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 31 Jul 2015 19:19:18 +0200

jbig2dec (0.12-1) unstable; urgency=medium

  * Update README.source to emphasize that control.in file is *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Update upstream URLs to reflect move to git.ghostscript.com and lack
    of tarball releases.
  * Declare compliance with Debian Policy 3.9.6.
  * Update Vcs-* fields.
  * Bump debhelper compatibility level to 9.
  * Update copyright info:
    + Extend coverage for myself.
    + Bump packaging license to GPL-3+.
    + Fix use SPDX shortname for X11 license.
      Thanks to Paul Richards Tagliamonte.
    + Use file format 1.0.
    + Use license short-name public-domain.
    + Bump main license to AGPL-3+.
      Add NEWS file about that change.
    + Drop unused Files and License sections for autotools files.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
  * Use newest autotools.
    Build-depend automake (not automake1.11) and on recent cdbs.
  * Drop patches 1002 1003 applied upstream.
  * Improve patch 1004: Remove extracted file from script to detect
    upstream code changes.
  * Add debian/patches/README documenting patch naming micro-policy.
  * Add patch 2001 to avoid including problematic and seemingly uneeded
    pngstruct.h.
  * Let CDBS move aside upstream cruft during build.
  * Cleanup more autotools files.
  * Add symbols file.
    Closes: bug#694899. Thanks to Logan Rosen.
  * Fix tie d-shlibs target also to development package (not only
    library package).
  * Add lintian overrides regarding license in License-Reference field.
    See bug#786450.
  * Update package relations:
    + Build-depend unversioned on d-shlibs: Needed version satisfied
      even in oldstable.
  * Install into multiarch paths.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 31 Jul 2015 11:45:03 +0200

jbig2dec (0.11+20120125-1) unstable; urgency=low

  * New snapshot of upstream git.
  * Autogenerate autotools.
  * Add patches cherry-picked from Ghostscript:
    1002: Prevent composition if src outside clip region.
    1003: Implement generic refinement region when TPGRON is TRUE.
  * Add patch 1004 to add config_types.h.in (not create in autogen.sh).
  * Fix strip editing noise from copyright file.
  * Fix watch file to cover current release: Ignore compression suffix.
  * Bump debhelper compat level to 7.
  * Bump standards-version to 3.9.2.
  * Simplify *.install file, thanks to debhelper compat level 7.
  * Ease building with git-buildpackage: Git-ignore .pc dir.
  * Update copyright file:
    + Reformat using rev. 174 of draft DEP-5 syntax.
    + Fix declare exceptions as such.
    + Fix include Expat~X license verbatim (adding "Some files
      differ..." to License field violates need for "verbatim copy").
    + Separate comments from License field in GNU License sections,
      shorten comments and quote license in them.
    + Rename licenses to better match recent DEP5 draft (e.g. avoid
      "other" prefix).
    + Rewrap License sections at 72 chars.
    + Fix reference GNU licenses versioned.
    + Document in Comment field of AFPL License section the lack of
      actual licensing text: license unused by Debian.
    + Extend copyright years.
  * Update package relations:
    + Relax build-depend unversioned on debhelper and devscripts (needed
      versions satisfied even in oldstable).
    + Build-depend on libtool, automake1.11 and autoconf.
    + Tighten build-dependency on d-shlibs.
  * Stop installing -la file.
    Closes bug#621683. Thanks to Neil Williams.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 10 Feb 2012 17:44:51 +0100

jbig2dec (0.11-1) unstable; urgency=low

  * Initial release. Closes: bug#539965.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 21 Apr 2010 21:06:47 +0200
