#!/usr/bin/make -f
# -*- makefile -*-

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

include /usr/share/dpkg/pkg-info.mk
include /usr/share/dpkg/vendor.mk
include /usr/share/dpkg/architecture.mk
SED_VERSION_SHORT := sed -re 's/([^.]+)\.([^.]+)\..*/\1.\2/'
RUST_VERSION := $(shell echo '$(DEB_VERSION_UPSTREAM)' | $(SED_VERSION_SHORT))
RUST_LONG_VERSION := $(shell echo '$(DEB_VERSION_UPSTREAM)' | sed -re 's/([^+]+).*/\1/')
LIBSTD_PKG := libstd-rust-$(RUST_VERSION)
# Sed expression that matches the "rustc" we have in our Build-Depends field
SED_RUSTC_BUILDDEP := sed -ne "/^Build-Depends:/,/^[^[:space:]\#]/{/^ *rustc:native .*,/p}" debian/control

# Version of /usr/bin/rustc
LOCAL_RUST_VERSION := $(shell rustc --version --verbose | sed -ne 's/^release: //p')

include /usr/share/dpkg/buildflags.mk
RUSTFLAGS = $(addprefix -C link-args=,$(LDFLAGS))

export CARGO_HOME = $(CURDIR)/debian/cargo_home

DEB_HOST_GNU_CPU ?= $(shell dpkg-architecture -qDEB_HOST_GNU_CPU)
#TRIPLET=$(DEB_HOST_GNU_CPU)-sun-solaris
TRIPLET=$(DEB_HOST_GNU_CPU)-unknown-illumos
DEB_BUILD_RUST_TYPE=$(TRIPLET)
DEB_HOST_RUST_TYPE=$(TRIPLET)
DEB_TARGET_RUST_TYPE=$(TRIPLET)
DEB_BUILD_GNU_TYPE=$(TRIPLET)
DEB_HOST_GNU_TYPE=$(TRIPLET)
DEB_TARGET_GNU_TYPE=$(TRIPLET)

DEB_DESTDIR := $(CURDIR)/debian/tmp

LLVM_VERSION = 11

BUILD_DOCS := true
ifneq (,$(findstring nodoc,$(DEB_BUILD_PROFILES)))
  BUILD_DOCS := false
endif
ifneq (,$(findstring nodoc,$(DEB_BUILD_OPTIONS)))
  BUILD_DOCS := false
endif

# Uncomment this to turn on verbose mode.
export DH_VERBOSE=1

# DilOS
#export DEB_BUILD_MAINT_OPTIONS = dilos=-saveargs,-ctf,-ld_z_ignore
export DEB_BUILD_MAINT_OPTIONS = dilos=-ld_z_ignore
#SYSTEM_WORKAROUNDS=LD_LIBRARY_PATH=/usr/gcc/8/lib/64

# Build products or non-source files in src/, that shouldn't go in rust-src
SRC_CLEAN = src/bootstrap/bootstrap.pyc \
        src/etc/__pycache__/

%:
	$(SYSTEM_WORKAROUNDS) dh $@ --parallel

.PHONY: build
build:
	$(SYSTEM_WORKAROUNDS) dh $@ --parallel


override_dh_clean:
	# Upstream contains a lot of these
	dh_clean -XCargo.toml.orig

debian/rust-src.%: debian/rust-src.%.in
	m4  -DRUST_LONG_VERSION="$(RUST_LONG_VERSION)" \
		"$<" > "$@"

override_dh_auto_build-arch: debian/dh_auto_build.stamp

override_dh_auto_configure:
	./configure --prefix=/usr \
		--enable-vendor --disable-extended \
		--build=$(TRIPLET) --target=$(TRIPLET) \
		--set target.$(TRIPLET).cc=gcc \
		--set target.$(TRIPLET).cxx=g++ \
		--set target.$(TRIPLET).linker=gcc \
		--disable-rpath --disable-ninja \
		--disable-codegen-tests \
		--enable-dist-src \
		--disable-llvm-static-stdcpp \
		--enable-docs \
		--release-channel=stable \
		--python=/usr/bin/python3 \
		--enable-llvm-link-shared \
		--llvm-config=/usr/lib/llvm-$(LLVM_VERSION)/bin/llvm-config \
		--enable-local-rebuild \
		--enable-local-rust
#		--local-rust-root=/opt/stage2

override_dh_auto_clean:
	$(RM) -rf build tmp .cargo debian/cargo_home config.stamp config.mk Makefile
	$(RM) -rf $(TEST_LOG) config.toml debian/rust-src.install debian/rust-src.links debian/*.stamp
	$(RM) -rf $(SRC_CLEAN) config.toml

debian/dh_auto_build.stamp:
	mkdir -p build && ln -sf ../stage0 build/cache
	$(MAKE)
ifeq (true,$(BUILD_DOCS))
	$(MAKE) doc
endif
	touch $@

debian/dh_auto_install.stamp:
	DESTDIR=$(DEB_DESTDIR) $(MAKE) install

	mkdir -p $(DEB_DESTDIR)/usr/lib/$(DEB_HOST_MULTIARCH)/
	mv $(DEB_DESTDIR)/usr/lib/lib*.so $(DEB_DESTDIR)/usr/lib/$(DEB_HOST_MULTIARCH)/

        # Replace duplicated compile-time/run-time dylibs with symlinks
	@set -e; \
	for f in $(DEB_DESTDIR)/usr/lib/rustlib/$(DEB_HOST_RUST_TYPE)/lib/lib*.so; do \
	  name=$${f##*/}; \
	  if [ -f "$(DEB_DESTDIR)/usr/lib/$(DEB_HOST_MULTIARCH)/$$name" ]; then \
	    echo "ln -sf ../../../$(DEB_HOST_MULTIARCH)/$$name $$f"; \
	    ln -sf ../../../$(DEB_HOST_MULTIARCH)/$$name $$f; \
	  fi; \
	done

	touch "$@"

override_dh_auto_install-arch: debian/dh_auto_install.stamp
override_dh_auto_install-indep: debian/dh_auto_install.stamp
ifeq (true,$(BUILD_DOCS))
	# Brute force to remove privacy-breach-logo lintian warning.
	# We could have updated the upstream sources but it would complexify
	# the rebase
	@set -e; \
	find $(DEB_DESTDIR)/usr/share/doc/*/html -iname '*.html' | \
	while read file; do \
	  topdir=$$(echo "$$file" | sed 's,^$(DEB_DESTDIR)/usr/share/doc/rust/html/,,; s,/[^/]*$$,/,; s,^[^/]*$$,,; s,[^/]\+/,../,g'); \
	  sed -i \
	    -e "s,https://\(doc\|www\).rust-lang.org/\(favicon.ico\|logos/rust-logo-32x32-blk.png\),$${topdir}rust-logo-32x32-blk.png," \
	    -e 's,<img src="https://img.shields.io/[^"]*" alt="\([^"]*\)" />,<span class="deb-privacy-replace--shields-io">\1</span>,g' "$$file"; \
	done
	find $(DEB_DESTDIR) \( -iname '*.html' -empty -o -name .lock -o -name '*.inc' \) -delete;
endif

override_dh_install-arch:
	dh_install
	dh_install -p$(LIBSTD_PKG) usr/lib/$(DEB_HOST_MULTIARCH)/
	dh_install -plibstd-rust-dev usr/lib/rustlib/$(DEB_HOST_RUST_TYPE)/lib/

override_dh_install-indep: debian/rust-src.install debian/rust-src.links
	dh_install
	chmod -x \
	  debian/rust-gdb/usr/share/rust-gdb/*.py
#	  debian/rust-lldb/usr/share/rust-lldb/*.py
	$(RM) -rf $(SRC_CLEAN:%=debian/rust-src/usr/src/rustc-$(RUST_LONG_VERSION)/%)
	# Get rid of lintian warnings
	find debian/rust-src/usr/src/rustc-$(RUST_LONG_VERSION) \
		\( -name .gitignore \
		-o -name 'LICENSE*' \
		-o -name 'LICENCE' \
		-o -name 'license' \
		-o -name 'COPYING*' \
		\) -delete
	# Remove files that autoload remote resources, caught by lintian
	$(RM) -rf debian/rust-src/usr/src/rustc-*/vendor/cssparser/docs/*.html
	$(RM) -rf debian/rust-src/usr/src/rustc-*/vendor/kuchiki/docs/*.html
	$(RM) -rf debian/rust-src/usr/src/rustc-*/vendor/url/docs/*.html
	$(RM) -rf debian/rust-src/usr/src/rustc-*/vendor/xz2/.gitmodules

override_dh_installchangelogs:
	dh_installchangelogs RELEASES.md

override_dh_installdocs:
	dh_installdocs -X.tex -X.aux -X.log -X.out -X.toc

override_dh_missing:
	dh_missing --list-missing

override_dh_compress:
	dh_compress -X.woff

override_dh_makeshlibs:
	dh_makeshlibs -V

	# dh_makeshlibs doesn't support our "libfoo-version.so" naming
	# structure, so we have to do this ourselves.
	install -o 0 -g 0 -d debian/$(LIBSTD_PKG)/DEBIAN
	LC_ALL=C ls debian/$(LIBSTD_PKG)/usr/lib/$(DEB_HOST_MULTIARCH)/lib*.so | \
	sed -n 's,^.*/\(lib.*\)-\(.\+\)\.so$$,\1 \2,p' | \
	while read name version; do \
	  echo "$$name $$version $(LIBSTD_PKG) (>= $(DEB_VERSION_UPSTREAM))"; \
	done > debian/$(LIBSTD_PKG)/DEBIAN/shlibs
	chmod 644 debian/$(LIBSTD_PKG)/DEBIAN/shlibs
	chown 0:0 debian/$(LIBSTD_PKG)/DEBIAN/shlibs

override_dh_shlibdeps:
	dh_shlibdeps -- -x$(LIBSTD_PKG)
