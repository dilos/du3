From: Chris Lamb <chris@chris-lamb.co.uk>
Date: Wed, 6 Feb 2019 14:47:41 +0100
Subject: Ensure the graphviz filenames are reproducible.

Whilst working on the Reproducible Builds effort [0], we noticed
that sphinx could generate output that is not reproducible.

In particular, the graphviz extension module would construct
filenames based on, inter alia, the contents of the `options`
dictionary.

As this contained the absolute build path of the source file
embedded in the `docname` variable this meant that builds of
documentation were not independent of where on a filesystem they
were built from.

Example filenames might be:

  -  html/_images/graphviz-9e71e0f9ba91d0842b51211b676ec4adb7e7afb8.png
  +  html/_images/graphviz-6241bbfd7ac6bd4e2ad9af451ab0dfb8719988d2.png

We fix this by limiting how much of the `docname` variable ends up
in the final constructed filename; I assume there is a good reason
for including the `options` dictionary in the first place, otherwise
we could simply omit it.

  [0] https://reproducible-builds.org
---
 sphinx/ext/graphviz.py | 4 +++-
 1 file changed, 3 insertions(+), 1 deletion(-)

diff --git a/sphinx/ext/graphviz.py b/sphinx/ext/graphviz.py
index b62d802..0e383db 100644
--- a/sphinx/ext/graphviz.py
+++ b/sphinx/ext/graphviz.py
@@ -216,7 +216,9 @@ def render_dot(self, code, options, format, prefix='graphviz'):
     # type: (nodes.NodeVisitor, unicode, Dict, unicode, unicode) -> Tuple[unicode, unicode]
     """Render graphviz code into a PNG or PDF output file."""
     graphviz_dot = options.get('graphviz_dot', self.builder.config.graphviz_dot)
-    hashkey = (code + str(options) + str(graphviz_dot) +
+    options_for_hash = options.copy()
+    options_for_hash = path.basename(options_for_hash.pop('docname', ''))
+    hashkey = (code + str(options_for_hash) + str(graphviz_dot) +
                str(self.builder.config.graphviz_dot_args)).encode('utf-8')
 
     fname = '%s-%s.%s' % (prefix, sha1(hashkey).hexdigest(), format)
