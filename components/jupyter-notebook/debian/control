Source: jupyter-notebook
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Gordon Ball <gordon@chronitis.net>, Jerome Benoit <calculus@rezozer.net>
Section: python
Priority: optional
Standards-Version: 4.3.0
Homepage: https://github.com/jupyter/notebook
Vcs-Git: https://salsa.debian.org/python-team/modules/jupyter-notebook.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/jupyter-notebook
Build-Depends: debhelper (>= 10),
               dh-python,
               python3-all,
               python3-setuptools,
               python3-nose <!nocheck>,
               python3-nose-exclude <!nocheck>,
               python3-requests <!nocheck>,
               python3-ipython (>= 4) <!nocheck>,
               python3-jupyter-core <!nocheck>,
               python3-jupyter-client <!nocheck>,
               python3-tornado <!nocheck>,
               python3-nbformat (>= 4.4) <!nocheck>,
               python3-nbconvert (>= 5) <!nocheck>,
               python3-ipykernel <!nocheck>,
               python3-prometheus-client <!nocheck>,
               python3-terminado (>= 0.8.1) <!nocheck>,
               python3-entrypoints <!nocheck>,
               python3-send2trash <!nocheck>,
               python3-zmq <!nocheck>,
               python-all,
               python-setuptools,
               python-nose <!nocheck>,
               python-nose-exclude <!nocheck>,
               python-requests <!nocheck>,
               python-ipython (>= 4) <!nocheck>,
               python-jupyter-core <!nocheck>,
               python-jupyter-client <!nocheck>,
               python-tornado <!nocheck>,
               python-nbformat (>= 4.4) <!nocheck>,
               python-nbconvert (>= 5) <!nocheck>,
               python-mock <!nocheck>,
               python-ipaddress <!nocheck>,
               python-ipykernel <!nocheck>,
               python-prometheus-client <!nocheck>,
               python-terminado (>= 0.8.1) <!nocheck>,
               python-entrypoints <!nocheck>,
               python-send2trash <!nocheck>,
               python-zmq <!nocheck>,
               python3-sphinx <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
               python3-nbsphinx <!nodoc>,
               libjs-backbone (>= 1.2),
               libjs-bootstrap (>= 3.3),
               libjs-bootstrap-tour (>= 0.9),
               libjs-codemirror,
               libjs-es6-promise (>= 1.0),
               fonts-font-awesome (>= 4.2),
               libjs-jed (>= 1.1.1),
               libjs-jquery,
               libjs-jquery-ui (>= 1.12),
               libjs-marked (>= 0.3),
               libjs-mathjax (>= 2.5),
               libjs-moment (>= 2.8.4),
               libjs-requirejs (>= 2.1),
               libjs-requirejs-text,
               libjs-text-encoding (>= 0.1),
               libjs-underscore (>= 1.5),
               libjs-jquery-typeahead (>= 2.0),
#               libjs-xterm (<< 3),
               libjs-term.js,
               nodejs,
               node-less (>= 1.5),
               node-source-map,
               node-requirejs (>= 2.3),
               node-uglify,
               node-po2json,
               pandoc [fixme]
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python-notebook
Architecture: all
Depends: ${misc:Depends},
         ${python:Depends},
         python-terminado (>= 0.3.3),
         libjs-backbone (>= 1.2),
         libjs-bootstrap (>= 3.3),
         libjs-bootstrap-tour (>= 0.9),
         libjs-codemirror,
         libjs-es6-promise (>= 1.0),
         fonts-font-awesome (>= 4.2),
         libjs-jed (>= 1.1.1),
         libjs-jquery,
         libjs-jquery-ui (>= 1.12),
         libjs-marked (>= 0.3),
         libjs-mathjax (>= 2.5),
         libjs-moment (>= 2.8.4),
         libjs-requirejs (>= 2.1),
         libjs-requirejs-text,
         libjs-text-encoding (>= 0.1),
         libjs-underscore (>= 1.5),
         libjs-jquery-typeahead (>= 2.0),
         libjs-term.js,
#         libjs-xterm (<< 3),
Recommends: python-ipywidgets,
            python-ipykernel
Suggests: python-notebook-doc
Built-Using: ${js:Built-Using}
Description: Jupyter interactive notebook (Python 2)
 The Jupyter Notebook is a web application that allows you to create and
 share documents that contain live code, equations, visualizations, and
 explanatory text. The Notebook has support for multiple programming
 languages, sharing, and interactive widgets.
 .
 This package contains the Python 2 library.
 .
 This package is not required to run Python 2 code in the notebook, only to
 allow Python 2 code to interact directly with the notebook server.

Package: python3-notebook
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-terminado (>= 0.3.3),
         libjs-backbone (>= 1.2),
         libjs-bootstrap (>= 3.3),
         libjs-bootstrap-tour (>= 0.9),
         libjs-codemirror,
         libjs-es6-promise (>= 1.0),
         fonts-font-awesome (>= 4.2),
         libjs-jed (>= 1.1.1),
         libjs-jquery,
         libjs-jquery-ui (>= 1.12),
         libjs-marked (>= 0.3),
         libjs-mathjax (>= 2.5),
         libjs-moment (>= 2.8.4),
         libjs-requirejs (>= 2.1),
         libjs-requirejs-text,
         libjs-text-encoding (>= 0.1),
         libjs-underscore (>= 1.5),
         libjs-jquery-typeahead (>= 2.0),
         libjs-term.js,
#         libjs-xterm (<< 3),
Recommends: python3-ipywidgets,
            python3-ipykernel
Suggests: python-notebook-doc
Built-Using: ${js:Built-Using}
Description: Jupyter interactive notebook (Python 3)
 The Jupyter Notebook is a web application that allows you to create and
 share documents that contain live code, equations, visualizations, and
 explanatory text. The Notebook has support for multiple programming
 languages, sharing, and interactive widgets.
 .
 This package contains the Python 3 library.

Package: python-notebook-doc
Build-Profiles: <!nodoc>
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
         libjs-mathjax
Built-Using: ${sphinxdoc:Built-Using}
Description: Jupyter interactive notebook (documentation)
 The Jupyter Notebook is a web application that allows you to create and
 share documents that contain live code, equations, visualizations, and
 explanatory text. The Notebook has support for multiple programming
 languages, sharing, and interactive widgets.
 .
 This package contains the documentation.

Package: jupyter-notebook
Architecture: all
Section: science
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-notebook (= ${binary:Version}),
         jupyter-core
Description: Jupyter interactive notebook
 The Jupyter Notebook is a web application that allows you to create and
 share documents that contain live code, equations, visualizations, and
 explanatory text. The Notebook has support for multiple programming
 languages, sharing, and interactive widgets.
 .
 This package provides the jupyter subcommands "notebook", "nbextension",
 "serverextension" and "bundlerextension".
