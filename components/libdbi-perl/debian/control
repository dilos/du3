Source: libdbi-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           Niko Tyni <ntyni@debian.org>,
           gregor herrmann <gregoa@debian.org>,
           Ansgar Burchardt <ansgar@debian.org>,
           Xavier Guimard <yadd@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper (>= 10),
               libtest-pod-coverage-perl,
               libtest-pod-perl,
               perl
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdbi-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdbi-perl.git
Homepage: https://dbi.perl.org/

Package: libdbi-perl
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Suggests: libclone-perl (>= 0.34),
          libmldbm-perl,
          libnet-daemon-perl,
          libsql-statement-perl (>= 1.402)
Breaks: libdbd-anydata-perl (<< 0.11+),
        libdbd-csv-perl (<< 0.3600+),
        libsql-statement-perl (<< 1.33+)
Provides: dh-sequence-perl-dbi,
          perl-dbdabi-${perl-dbdabi-version}
Description: Perl Database Interface (DBI)
 DBI (DataBase Interface) is a Perl framework that provides a common interface
 to access various backend databases in a uniform manner. DBD (DataBase Driver)
 modules provide implementations for various backend data storage mechanisms
 including networked relational databases (particularly SQL databases) and even
 web services such as the Google search engine.
 .
 It is extremely portable and available for a wide range of operating systems,
 architectures and data stores, including:
 .
  * Oracle
  * Microsoft SQL Server
  * IBM DB2
  * SQLite
  * PostgreSQL
  * Firebird
  * MySQL
