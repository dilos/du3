Source: libheif
Section: libs
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Joachim Bauch <bauch@struktur.de>
Build-Depends:
 debhelper (>= 10),
 libde265-dev,
 libjpeg-dev,
 libpng-dev,
 libx265-dev,
 pkg-config
Standards-Version: 4.1.4
Homepage: http://www.libheif.org
Vcs-Git: https://salsa.debian.org/multimedia-team/libheif.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/libheif

Package: libheif1
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: ISO/IEC 23008-12:2017 HEIF file format decoder - shared library
 libheif is an ISO/IEC 23008-12:2017 HEIF file format decoder. HEIF is a new
 image file format employing HEVC (h.265) image coding for the best compression
 ratios currently possible.
 .
 This package contains the shared library.

Package: libheif-dev
Section: libdevel
Multi-Arch: same
Architecture: any
Depends:
 libheif1 (= ${binary:Version}),
 ${misc:Depends}
Description: ISO/IEC 23008-12:2017 HEIF file format decoder - development files
 libheif is an ISO/IEC 23008-12:2017 HEIF file format decoder. HEIF is a new
 image file format employing HEVC (h.265) image coding for the best compression
 ratios currently possible.
 .
 The development headers for compiling programs that use libheif are provided
 by this package.

Package: libheif-examples
Section: video
Architecture: any
Depends:
 libheif1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Description: ISO/IEC 23008-12:2017 HEIF file format decoder - examples
 libheif is an ISO/IEC 23008-12:2017 HEIF file format decoder. HEIF is a new
 image file format employing HEVC (h.265) image coding for the best compression
 ratios currently possible.
 .
 Sample applications using libheif are provided by this package.

Package: heif-thumbnailer
Section: graphics
Architecture: any
Depends:
 libheif1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Description: ISO/IEC 23008-12:2017 HEIF file format decoder - thumbnailer
 libheif is an ISO/IEC 23008-12:2017 HEIF file format decoder. HEIF is a new
 image file format employing HEVC (h.265) image coding for the best compression
 ratios currently possible.
 .
 A thumbnailer for HEIF images that can be used by Nautilus is provided by this
 package.
