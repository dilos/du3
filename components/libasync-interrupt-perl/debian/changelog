libasync-interrupt-perl (1.24-1+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Tue, 02 Jun 2020 14:23:26 +0300

libasync-interrupt-perl (1.24-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * New upstream release.
  * Update build dependencies.
  * Declare compliance with Debian Policy 4.1.4.

 -- gregor herrmann <gregoa@debian.org>  Fri, 11 May 2018 16:50:33 +0200

libasync-interrupt-perl (1.22-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Jotam Jr. Trejo from Uploaders. Thanks for your work!

  * New upstream release.
  * Declare compliance with Debian Policy 4.1.3.
  * Bump debhelper compatibility level to 10.
  * Add /me to Uploaders.
  * Set bindnow linker flag in debian/rules.
  * Use HTTPS for some URLs in debian/copyright. Thanks to duck.

 -- gregor herrmann <gregoa@debian.org>  Fri, 19 Jan 2018 19:31:32 +0100

libasync-interrupt-perl (1.21-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 1.21
  * Declare compliance with Debian policy 3.9.6
  * Add autopkgtest-pkg-perl
  * Fix perl path in lintian.override
  * Update upstream copyright

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sat, 08 Aug 2015 18:51:15 -0300

libasync-interrupt-perl (1.20-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: use Comment instead of the deprecated X-Comment.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 10 May 2014 18:00:33 +0200

libasync-interrupt-perl (1.10-1) unstable; urgency=low

  [ Fabrizio Regalli ]
  * Bump to 3.9.2 Standard-Version.
  * Add myself to Uploaders and Copyright.
  * Switch d/compat to 8.
  * Build-Depends: switch to debhelper (>= 8).
  * Fixed lintian copyright-refers-to-symlink-license message.
  * Updated Format-Specification url in d/copyright.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * debian/watch: pad minor version to cover single digits.

  [ Jotam Jr. Trejo ]
  * Imported Upstream version 1.10
  * Changed debhelper compatibility level to 9
  * debian/control: updated required debhelper version to 9
  * debian/copyright: refreshed the format to use copyright-format 1.0
  * Updated the Standards Version to 3.9.3
    + Added lintian override to disable a no fortified functions
      warning since we are using the -D_FORTIFY_SOURCE=2 flag at build
      time
  * Added myself to Uploaders and Copyright

 -- Jotam Jr. Trejo <jotamjr@debian.org.sv>  Tue, 19 Jun 2012 22:14:01 -0600

libasync-interrupt-perl (1.05-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 15 May 2010 09:42:49 -0400

libasync-interrupt-perl (1.04-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Standards-Version 3.8.4 (no changes)
  * Use new DEP5 copyright format
  * Rewrite control description

  [ gregor herrmann ]
  * Convert to source format 3.0 (quilt).

 -- Jonathan Yu <jawnsy@cpan.org>  Fri, 02 Apr 2010 23:21:19 -0400

libasync-interrupt-perl (1.02-1) unstable; urgency=low

  * Initial Release (Closes: #544718)

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 03 Sep 2009 10:45:40 -0400
