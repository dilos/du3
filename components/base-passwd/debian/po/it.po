# Italian translation of base-passwd debconf messages
# Copyright (C) 2014, base-passwd package copyright holder
# This file is distributed under the same license as the base-passwd package.
# Beatrice Torracca <beatricet@libero.it>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: base-passwd\n"
"Report-Msgid-Bugs-To: base-passwd@packages.debian.org\n"
"POT-Creation-Date: 2014-01-05 13:38-0800\n"
"PO-Revision-Date: 2014-03-28 18:33+0200\n"
"Last-Translator: Beatrice Torracca <beatricet@libero.it>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Do you want to move the user ${name}?"
msgstr "Spostare l'utente ${name}?"

#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#: ../templates:1001 ../templates:2001 ../templates:3001 ../templates:4001
#: ../templates:5001 ../templates:6001 ../templates:7001 ../templates:8001
#: ../templates:9001 ../templates:10001 ../templates:11001 ../templates:12001
msgid ""
"update-passwd has found a difference between your system accounts and the "
"current Debian defaults.  It is advisable to allow update-passwd to change "
"your system; without those changes some packages might not work correctly.  "
"For more documentation on the Debian account policies, please see /usr/share/"
"doc/base-passwd/README."
msgstr ""
"update-passwd ha trovato una differenza tra gli account del sistema e i "
"valori predefiniti attuali di Debian. È consigliabile permettere ad update-"
"passwd di cambiare il sistema; senza tali cambiamenti alcuni pacchetti "
"potrebbero non funzionare correttamente. Per maggiore documentazione sulle "
"politiche Debian per gli account, vedere /usr/share/doc/base-passwd/README."

#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#: ../templates:1001 ../templates:2001 ../templates:3001 ../templates:4001
#: ../templates:5001 ../templates:6001 ../templates:7001 ../templates:8001
#: ../templates:9001 ../templates:10001 ../templates:11001 ../templates:12001
msgid "The proposed change is:"
msgstr "La modifica proposta è:"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Move user \"${name}\" (${id}) to before the NIS compat \"+\" entry"
msgstr "Spostare l'utente «${name}» (${id}) prima della voce NIS compat «+»"

#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. Type: boolean
#. Description
#: ../templates:1001 ../templates:2001 ../templates:3001 ../templates:4001
#: ../templates:5001 ../templates:6001 ../templates:7001 ../templates:8001
#: ../templates:9001 ../templates:10001 ../templates:11001 ../templates:12001
msgid ""
"If you allow this change, a backup of modified files will be made with the "
"extension .org, which you can use if necessary to restore the current "
"settings.  If you do not make this change now, you can make it later with "
"the update-passwd utility."
msgstr ""
"Se si permette questa modifica, verrà creato un backup dei file modificati "
"con l'estensione .org che può essere usato, se necessario, per ripristinare "
"le impostazioni attuali. Se non si effettua questa modifica ora, è possibile "
"farlo in un secondo momento con l'utilità update-passwd."

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Do you want to move the group ${name}?"
msgstr "Spostare il gruppo ${name}?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Move group \"${name}\" (${id}) to before the NIS compat \"+\" entry"
msgstr "Spostare il gruppo «${name}» (${id}) prima della voce NIS compat «+»"

#. Type: boolean
#. Description
#: ../templates:3001
msgid "Do you want to add the user ${name}?"
msgstr "Aggiungere l'utente ${name}?"

#. Type: boolean
#. Description
#: ../templates:3001
msgid "Add user \"${name}\" (${id})"
msgstr "Aggiungere l'utente «${name}» (${id})"

#. Type: boolean
#. Description
#: ../templates:4001
msgid "Do you want to add the group ${name}?"
msgstr "Aggiungere il gruppo ${name}?"

#. Type: boolean
#. Description
#: ../templates:4001
msgid "Add group \"${name}\" (${id})"
msgstr "Aggiungere il gruppo «${name}» (${id})"

#. Type: boolean
#. Description
#: ../templates:5001
msgid "Do you want to remove the user ${name}?"
msgstr "Rimuovere l'utente ${name}?"

#. Type: boolean
#. Description
#: ../templates:5001
msgid "Remove user \"${name}\" (${id})"
msgstr "Rimuovere l'utente «${name}» (${id})"

#. Type: boolean
#. Description
#: ../templates:6001
msgid "Do you want to remove the group ${name}?"
msgstr "Rimuovere il gruppo ${name}?"

#. Type: boolean
#. Description
#: ../templates:6001
msgid "Remove group \"${name}\" (${id})"
msgstr "Rimuovere il gruppo «${name}» (${id})"

#. Type: boolean
#. Description
#: ../templates:7001
msgid "Do you want to change the UID of user ${name}?"
msgstr "Cambiare l'UID dell'utente ${name}?"

#. Type: boolean
#. Description
#: ../templates:7001
msgid "Change the UID of user \"${name}\" from ${old_uid} to ${new_uid}"
msgstr "Cambiare l'UID dell'utente «${name}» da ${old_uid} a ${new_uid}"

#. Type: boolean
#. Description
#: ../templates:8001
msgid "Do you want to change the GID of user ${name}?"
msgstr "Cambiare il GID dell'utente ${name}?"

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Change the GID of user \"${name}\" from ${old_gid} (${old_group}) to "
"${new_gid} (${new_group})"
msgstr ""
"Cambiare il GID dell'utente «${name}» da ${old_gid} (${old_group}) a "
"${new_gid} (${new_group})"

#. Type: boolean
#. Description
#: ../templates:9001
msgid "Do you want to change the GECOS of user ${name}?"
msgstr "Cambiare il GECOS dell'utente ${name}?"

#. Type: boolean
#. Description
#: ../templates:9001
msgid ""
"Change the GECOS of user \"${name}\" from \"${old_gecos}\" to "
"\"${new_gecos}\""
msgstr ""
"Cambiare il GECOS dell'utente «${name}» da \"${old_gecos}\" a "
"\"${new_gecos}\""

#. Type: boolean
#. Description
#: ../templates:10001
msgid "Do you want to change the home directory of user ${name}?"
msgstr "Cambiare la directory home dell'utente ${name}?"

#. Type: boolean
#. Description
#: ../templates:10001
msgid ""
"Change the home directory of user \"${name}\" from ${old_home} to ${new_home}"
msgstr ""
"Cambiare la directory home dell'utente «${name}» da ${old_home} a ${new_home}"

#. Type: boolean
#. Description
#: ../templates:11001
msgid "Do you want to change the shell of user ${name}?"
msgstr "Cambiare la shell dell'utente ${name}?"

#. Type: boolean
#. Description
#: ../templates:11001
msgid "Change the shell of user \"${name}\" from ${old_shell} to ${new_shell}"
msgstr "Cambiare la shell dell'utente «${name}» da ${old_shell} a ${new_shell}"

#. Type: boolean
#. Description
#: ../templates:12001
msgid "Do you want to change the GID of group ${name}?"
msgstr "Cambiare il GID del gruppo ${name}?"

#. Type: boolean
#. Description
#: ../templates:12001
msgid "Change the GID of group \"${name}\" from ${old_gid} to ${new_gid}"
msgstr "Cambiare il GID del gruppo «${name}» da ${old_gid} a ${new_gid}"
