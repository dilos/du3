#!/bin/sh

#DEBHELPER#

set -e
if [ "${BASEDIR:=/}" != "/" ]; then
	exit 0
fi

pam-auth-update --package

PACKAGE=libpam-ldap
CONFFILE="/etc/pam_ldap.conf"
PASSWDFILE="/etc/pam_ldap.secret"
OLDPASSWDFILE="/etc/ldap.secret"

add_missing()
{
	# FIXME: it would be nice to get the prototype from a template.

	parameter=$1
	value=$2
	echo "$parameter $value" >> $CONFFILE
}

change_value()
{
	parameter=$1
	value=$2
	commented=0 ; notthere=0
	egrep -i -q "^$parameter " $CONFFILE || notthere=1
	if [ "$notthere" = "1" ]; then
		if ( egrep -i -q "^# *$parameter" $CONFFILE ); then
			notthere=0
			commented=1
		fi
	fi

	if [ "$notthere" = "1" ]; then
		add_missing $parameter $value
	else
		# i really need a better way to do this...
		# currently we replace only the first match, we need a better
		# way of dealing with multiple hits.
		if [ "$commented" = "1" ]; then
			value="$value" parameter="$parameter" perl -i -p -e 's/^# *\Q$ENV{"parameter"}\E .*/$ENV{"parameter"} $ENV{"value"}/i
				and $match=1 unless ($match)' $CONFFILE
		else
			value="$value" parameter="$parameter" perl -i -p -e 's/^\Q$ENV{"parameter"}\E .*/$ENV{"parameter"} $ENV{"value"}/i
				and $match=1 unless ($match)' $CONFFILE
		fi
	fi
}

disable_param()
{
	parameter=$1
	enabled=0
	egrep -q "^$parameter " $CONFFILE && enabled=1
	if [ "$enabled" = "1" ]; then
		perl -i -p -e "s/^($parameter .*)/#\$1/i" $CONFFILE
	fi
}
	


# ok, lets get to business..
. /usr/share/debconf/confmodule

# lets create the configuration from example if it's not there.
examplefile=/usr/share/$PACKAGE/ldap.conf
if [ ! -e $CONFFILE -a -e $examplefile ]; then
	cat > $CONFFILE << EOM
###DEBCONF###
# the configuration of this file will be done by debconf as long as the
# first line of the file says '###DEBCONF###'
#
# you should use dpkg-reconfigure to configure this file
#
EOM
	cat $examplefile >> $CONFFILE
	chmod 0644 $CONFFILE
	db_set libpam-ldap/override true
fi

db_get libpam-ldap/override
if [ "$RET" = "true" ]; then
	if ( head -1 $CONFFILE | grep -q -v '^###DEBCONF###$' ); then
		mv $CONFFILE $CONFFILE.tmp
		cat > $CONFFILE << EOM
###DEBCONF###
EOM
		cat $CONFFILE.tmp >> $CONFFILE
		rm -f $CONFFILE.tmp
		chmod 0644 $CONFFILE
	fi

	db_get shared/ldapns/ldap-server
	if echo $RET | egrep -q '^ldap[is]?://'; then
		disable_param host
		change_value uri "$RET"
	else
		disable_param uri
		change_value host "$RET"
	fi

	db_get shared/ldapns/base-dn
	change_value base "$RET"

	db_get shared/ldapns/ldap_version
	change_value ldap_version "$RET"

	db_get libpam-ldap/pam_password
	change_value pam_password "$RET"

	db_get libpam-ldap/dbrootlogin
	if [ "$RET" = "true" ]; then
		# separate root login to the database
		db_get libpam-ldap/rootbinddn
		change_value rootbinddn "$RET"

		db_get libpam-ldap/rootbindpw
		if [ "$RET" != "" ]; then
			rm -f $PASSWDFILE $OLDPASSWDFILE
			echo $RET > $PASSWDFILE
			chmod 0600 $PASSWDFILE
			db_set libpam-ldap/rootbindpw ''
		else
			# copy the old password file to its new location
			if [ ! -e $PASSWDFILE -a -e $OLDPASSWDFILE ]; then
				cp -a $OLDPASSWDFILE $PASSWDFILE
			fi
		fi
	else
		# ok, so the user refused to use this feature, better make
		# sure it's really off.
		disable_param rootbinddn
		rm -f $PASSWDFILE /etc/ldap.conf
	fi

	db_get libpam-ldap/dblogin
	if [ "$RET" = "true" ]; then
		# user wants to log in to the database, so be it.
		db_get libpam-ldap/binddn
		change_value binddn "$RET"

		db_get libpam-ldap/bindpw
		if [ "$RET" != "" ]; then
			change_value bindpw "$RET"
			db_set libpam-ldap/bindpw ''
		fi
	else
		# once again, user didn't.. lets make sure we dont.
		disable_param binddn
		disable_param bindpw
	fi
else
	# copy the password file to its new location
	if [ ! -e $PASSWDFILE -a -e $OLDPASSWDFILE ]; then
		cp -a $OLDPASSWDFILE $PASSWDFILE
	fi
fi
db_stop
