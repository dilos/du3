libgusb (0.3.0-1+dilos2) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Tue, 07 Sep 2021 16:56:56 +0300

libgusb (0.3.0-1) unstable; urgency=medium

  [ Sebastien Bacher ]
  * New upstream version (Closes: #907565).
  * debian/control, debian/libgusb-dev.install, debian/rules:
    - update for the build system change
  * debian/control, debian/compat, debian/rules:
    - use compat 11
  * debian/docs: renamed README following upstream, remove TODO

  [ Michal Čihař ]
  * Cleanup debian/control using cme.
  * Move git repository to salsa.debian.org.
  * Strict versioned dependency on gir typelib.

 -- Michal Čihař <nijel@debian.org>  Mon, 15 Oct 2018 16:41:42 +0200

libgusb (0.2.11-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Mon, 07 Aug 2017 12:55:14 -0400

libgusb (0.2.10-2) unstable; urgency=medium

  * Revert upstream changes in symbol versioning (Closes: #868340).

 -- Michal Čihař <nijel@debian.org>  Mon, 17 Jul 2017 11:21:06 +0200

libgusb (0.2.10-1) unstable; urgency=medium

  * New upstream release.
  * Ensure the documentation is built during package build.
  * Bump standards to 4.0.0.

 -- Michal Čihař <nijel@debian.org>  Mon, 10 Jul 2017 20:03:19 +0200

libgusb (0.2.9-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards to 3.9.7.
  * Ship gusbcmd in the libgusb-dev package.

 -- Michal Čihař <nijel@debian.org>  Mon, 04 Apr 2016 22:00:53 +0200

libgusb (0.2.8-1) unstable; urgency=medium

  * New upstream release.
  * Update symbols file.

 -- Michal Čihař <nijel@debian.org>  Wed, 02 Dec 2015 08:28:53 +0100

libgusb (0.2.7-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Thu, 17 Sep 2015 09:30:23 +0200

libgusb (0.2.6-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Thu, 23 Jul 2015 08:03:24 +0200

libgusb (0.2.5-1) unstable; urgency=medium

  * New upstream release.
  * Update symbols file as the library started to support versioned symbols.

 -- Michal Čihař <nijel@debian.org>  Mon, 08 Jun 2015 10:30:12 +0200

libgusb (0.2.4-1) unstable; urgency=medium

  * New upstream release.
  * Raised glib dependency to 2.38.
  * New exported symbols.

 -- Michal Čihař <nijel@debian.org>  Mon, 16 Mar 2015 08:43:22 +0100

libgusb (0.2.2-1) unstable; urgency=medium

  * New upstream release.
    - Dropped gudev dependency, raised glib and libusb ones.
    - Updated symbols table.
  * Bump standards to 3.9.6.

 -- Michal Čihař <nijel@debian.org>  Thu, 27 Nov 2014 14:44:48 +0100

libgusb (0.1.6-5) unstable; urgency=medium

  * Miscellaneous fixes from Ubuntu (Closes: #736827):
    * Build-depend on valac (>= 0.20), not valac-0.20.
    * Install typelib to /usr/lib as expected, not to the multiarch dir.
    * Keep libgusb2.symbols at version 0.1.5 for compatibility with
      existing Ubuntu builds.
    * Call dh --with gir.

 -- Michal Čihař <nijel@debian.org>  Mon, 27 Jan 2014 11:42:27 +0100

libgusb (0.1.6-4) unstable; urgency=medium

  * Use dh-autoreconf for newer libtool.

 -- Michal Čihař <nijel@debian.org>  Mon, 20 Jan 2014 16:04:03 +0100

libgusb (0.1.6-3) unstable; urgency=medium

  * Bump standards to 3.9.5.
  * Use debhelper 9.
  * Use canonical VCS URLs in debian/control.
  * Enable multiarch support.

 -- Michal Čihař <nijel@debian.org>  Mon, 20 Jan 2014 15:49:26 +0100

libgusb (0.1.6-2) unstable; urgency=low

  * Build with valac-0.20 (Closes: #707465).

 -- Michal Čihař <nijel@debian.org>  Fri, 17 May 2013 10:38:30 +0200

libgusb (0.1.6-1) unstable; urgency=low

  * Update to unstable.

 -- Michal Čihař <nijel@debian.org>  Tue, 07 May 2013 09:53:03 +0200

libgusb (0.1.6-0.1) experimental; urgency=low

  * Non maintainer upload with maintainer approval
  * Imported Upstream version 0.1.6 (Closes: #699941)
  * Update build-dependencies according to configure.ac
    - added libglib2.0-dev, gobject-introspection and valac
  * Add GObject introspection package gir1.2-gusb-1.0
  * Install Vala API (vapi) in libgusb-dev package
  * libgusb2.symbols: update for added symbols

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 08 Mar 2013 09:52:33 +0100

libgusb (0.1.3-5) unstable; urgency=low

  * Fix upstream homepage link.

 -- Michal Čihař <nijel@debian.org>  Mon, 25 Jun 2012 15:08:44 +0200

libgusb (0.1.3-4) unstable; urgency=low

  * Improve package description (Closes: #668043).
  * Adjust debian/copyright to final version of DEP-5.
  * Bump standards to 3.9.3.

 -- Michal Čihař <nijel@debian.org>  Mon, 14 May 2012 10:16:54 +0200

libgusb (0.1.3-3) unstable; urgency=low

  * Add missing dependencies to devel package.

 -- Michal Čihař <nijel@debian.org>  Wed, 04 Jan 2012 12:27:11 +0100

libgusb (0.1.3-2) unstable; urgency=low

  * Disable tests completely, they need USB access and it fails on most
    buildds.

 -- Michal Čihař <nijel@debian.org>  Wed, 04 Jan 2012 07:57:37 +0100

libgusb (0.1.3-1) unstable; urgency=low

  * Initial release (Closes: #654204).

 -- Michal Čihař <nijel@debian.org>  Mon, 02 Jan 2012 13:50:33 +0100
