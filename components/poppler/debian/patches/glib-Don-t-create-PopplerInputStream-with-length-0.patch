From: Simon McVittie <smcv@debian.org>
Date: Thu, 14 Feb 2019 09:43:32 +0000
Subject: glib: Don't create PopplerInputStream with length 0

Since commit a59f6164, PopplerInputStream requires a nonzero length.

Loosely based on an earlier patch by Kouhei Sutou. This version adds
support for length == -1, which is documented to work.

Bug: https://gitlab.freedesktop.org/poppler/poppler/issues/414
Bug-Debian: https://bugs.debian.org/896596
Forwarded: https://gitlab.freedesktop.org/poppler/poppler/merge_requests/189
---
 glib/poppler-document.cc | 9 ++++++++-
 1 file changed, 8 insertions(+), 1 deletion(-)

diff --git a/glib/poppler-document.cc b/glib/poppler-document.cc
index ed37da4c..e04c8b42 100644
--- a/glib/poppler-document.cc
+++ b/glib/poppler-document.cc
@@ -309,7 +309,14 @@ poppler_document_new_from_stream (GInputStream *stream,
   }
 
   if (stream_is_memory_buffer_or_local_file(stream)) {
-    str = new PopplerInputStream(stream, cancellable, 0, false, 0, Object(objNull));
+    if (length == (goffset)-1) {
+      if (!g_seekable_seek(G_SEEKABLE(stream), 0, G_SEEK_END, cancellable, error)) {
+        g_prefix_error(error, "Unable to determine length of stream: ");
+        return nullptr;
+      }
+      length = g_seekable_tell(G_SEEKABLE(stream));
+    }
+    str = new PopplerInputStream(stream, cancellable, 0, false, length, Object(objNull));
   } else {
     CachedFile *cachedFile = new CachedFile(new PopplerCachedFileLoader(stream, cancellable, length), new GooString());
     str = new CachedFileStream(cachedFile, 0, false, cachedFile->getLength(), Object(objNull));
