#!/bin/sh

set -e
if [ "${BASEDIR:=/}" = "/" ]; then
	BASEDIR=""
fi

VERSION=${DPKG_MAINTSCRIPT_PACKAGE##*-}

clean_dir() {
    if [ -d "$1" ] && [ ! -L "$1" ]; then
        rmdir "$1" >/dev/null 2>/dev/null || true
    fi
}

drop_cluster() {
    # if we still have the postgresql-common package, use it to also shutdown
    # server, etc.; otherwise just remove the directories
    if [ -x ${BASEDIR}/usr/bin/pg_dropcluster ]; then
        pg_dropcluster --stop-server $VERSION "$1"
    else
        # remove data directory
        PGDATALINK="/etc/postgresql/$VERSION/$1/pgdata"
        if [ -e "${BASEDIR}$PGDATALINK" ]; then
            rm -rf $(readlink -f "${BASEDIR}$PGDATALINK") "${BASEDIR}$PGDATALINK"
        else
            rm -rf "${BASEDIR}/var/lib/postgresql/$VERSION/$1/"
        fi

        # remove log file, including rotated ones
        LOGLINK="/etc/postgresql/$VERSION/$1/log"
        if [ -e "${BASEDIR}$LOGLINK" ]; then
            LOG=$(readlink -f "${BASEDIR}$LOGLINK")
            rm -f ${BASEDIR}$LOG* "${BASEDIR}$LOGLINK"
        else
            rm -f ${BASEDIR}/var/log/postgresql/postgresql-$VERSION-"$1".log*
        fi

        # remove conffiles
        for f in pg_hba.conf pg_ident.conf postgresql.conf start.conf environment pg_ctl.conf; do
            rm -f ${BASEDIR}/etc/postgresql/$VERSION/"$1"/$f
        done
        # remove empty conf.d directories
        for d in /etc/postgresql/$VERSION/"$1"/*/; do
            clean_dir "${BASEDIR}$d"
        done

        clean_dir ${BASEDIR}/etc/postgresql/$VERSION/"$1"
    fi
}

purge_package () {
    # ask the user if they want to remove clusters. If debconf is not
    # available, just remove everything
    if [ -e ${BASEDIR}/usr/share/debconf/confmodule ]; then
        db_set $DPKG_MAINTSCRIPT_PACKAGE/postrm_purge_data true
        db_input high $DPKG_MAINTSCRIPT_PACKAGE/postrm_purge_data || :
        db_go || :
        db_get $DPKG_MAINTSCRIPT_PACKAGE/postrm_purge_data || :
        [ "$RET" = "false" ] && return 0
    fi

    for c in /etc/postgresql/$VERSION/*; do
        [ -e "${BASEDIR}$c/postgresql.conf" ] || continue
        cluster=$(basename "$c")
        echo "Dropping cluster $cluster..."
        drop_cluster "$cluster"
    done

    clean_dir ${BASEDIR}/etc/postgresql/$VERSION
    clean_dir ${BASEDIR}/var/lib/postgresql/$VERSION
    clean_dir ${BASEDIR}/var/log/postgresql/$VERSION
}

if [ "$1" = purge ] && [ -d "${BASEDIR}/etc/postgresql/$VERSION" ] && [ "$(ls ${BASEDIR}/etc/postgresql/$VERSION)" ]; then
    # can't load debconf from a function
    if [ -e ${BASEDIR}/usr/share/debconf/confmodule ]; then
        . ${BASEDIR}/usr/share/debconf/confmodule
    fi
    purge_package
fi

#DEBHELPER#
