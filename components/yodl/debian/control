Source: yodl
Section: text
Priority: optional
Maintainer: Frank B. Brokken <f.b.brokken@rug.nl>
Uploaders: George Danchev <danchev@spnet.net>,
           tony mancill <tmancill@debian.org>
Standards-Version: 4.1.4
Homepage: https://gitlab.com/fbb-git/yodl
Build-Depends: debhelper (>= 11), icmake (>= 9.02.02),
 texlive-latex-base, texlive-generic-recommended,
 texlive-latex-recommended, texlive-fonts-recommended,
 texlive-latex-extra,  ghostscript
Build-Depends-Indep: cm-super-minimal
Vcs-Browser: https://salsa.debian.org/debian/yodl
Vcs-Git: https://salsa.debian.org/debian/yodl.git

Package: yodl
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: yodl-doc
Description: Your Own Document Language (Yodl) is a pre-document language
 Yodl is a package that implements a pre-document language and tools to
 process it.  The idea of Yodl is that you write up a document in a
 pre-language, then use the tools (e.g. yodl2html) to convert it to some
 final document language.  Current converters are for HTML, man, LaTeX
 SGML and texinfo, a poor-man's text converter and an experimental xml
 converter. Main document types are
 "article", "report", "book", "manpage" and "letter".
 The Yodl document language was  designed to be easy to use and extensible.

Package: yodl-doc
Architecture: all
Section: doc
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Documentation for Your Own Document Language (Yodl)
 Yodl is a package that implements a pre-document language and tools to
 process it.  The idea of Yodl is that you write up a document in a
 pre-language, then use the tools (e.g. yodl2html) to convert it to some
 final document language.  Current converters are for HTML, man, LaTeX
 SGML and texinfo, a poor-man's text converter and an experimental xml
 converter. Main document types are
 "article", "report", "book", "manpage" and "letter".
 The Yodl document language was  designed to be easy to use and extensible.
 .
 This package provides the supplemental documentation for Yodl.
