#!/usr/bin/make -f

ORIG_SRC_VERSION = 6.1.2

DPKG_EXPORT_BUILDFLAGS = 1
export DEB_BUILD_MAINT_OPTIONS=dilos=-saveargs
include /usr/share/dpkg/buildflags.mk

# Uncomment this to turn on verbose mode. 
#export DH_VERBOSE=1

# This has to be exported to make some magic below work.
export DH_OPTIONS

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
  NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
  JOBSFLAG = -j$(NUMJOBS)
endif

DEB_HOST_ARCH  ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)
export DEB_HOST_GNU_TYPE  := $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
export DEB_BUILD_GNU_TYPE := $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
export DEB_HOST_MULTIARCH := $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

MAKE_CHECK = : disabled make check
ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
  ifeq ($(DEB_BUILD_GNU_TYPE), $(DEB_HOST_GNU_TYPE))
    MAKE_CHECK = $(MAKE) check
  endif
endif

#export CFLAGS = -Wall -g 
#export LDFLAGS = -Wl,-Bsymbolic-functions

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
  CFLAGS += -O0
else
  ifneq (,$(findstring $(DEB_HOST_ARCH), ia64))
    CFLAGS += -O2
  else
    CFLAGS += -O3
  endif
endif

SAVEARGS=
ifeq ($(DEB_HOST_ARCH),solaris-i386)
SAVEARGS += -msave-args
endif

confflags = --prefix=/usr --enable-cxx

ifneq (,$(filter $(DEB_HOST_ARCH), amd64 kfreebsd-amd64 lpia solaris-i386))
  confflags += --disable-fat
else
  confflags += --enable-fat
endif

ifeq ($(DEB_BUILD_GNU_TYPE), $(DEB_HOST_GNU_TYPE))
  AR = ar
  confflags_build += --build $(DEB_BUILD_GNU_TYPE)
else
  AR = $(DEB_HOST_GNU_TYPE)-ar
  confflags_build += --build $(DEB_BUILD_GNU_TYPE) --host $(DEB_HOST_GNU_TYPE) --target $(DEB_HOST_GNU_TYPE)
endif

ifneq (,$(findstring $(DEB_HOST_ARCH), x32))
  confflags += ABI=x32
endif

ifneq (,$(filter $(DEB_HOST_ARCH), mips64 mips64el))
  confflags += ABI=64
endif

# set MACH from uname -p to either sparc or i386
MACH := $(shell uname -p)

# assembler detection for GNU MP and GNU MPFR is done via MPN_PATH
# at ./configure time. GNU MPFR wants to know GNU MP's MPN_PATH.
MPN32_i386 = x86/pentium x86 generic
MPN64_i386 = x86_64/pentium4 x86_64 generic
MPN32_sparc = sparc32/v9 sparc32 generic
MPN64_sparc = sparc64 generic
MPN_32 = $(MPN32_$(MACH))
MPN_64 = $(MPN64_$(MACH))

#confflags_ma = $(confflags) $(confflags_build) --libdir=/usr/lib/$(DEB_HOST_MULTIARCH)
confflags_ma = $(confflags) $(confflags_build)

#CC   = $(DEB_HOST_GNU_TYPE)-gcc
#CXX   = $(DEB_HOST_GNU_TYPE)-g++
CC   = gcc
CXX   = g++


get-orig-source: gmp-$(ORIG_SRC_VERSION).tar.xz
	tar --xz -xf $<
	mv gmp-$(ORIG_SRC_VERSION) gmp-$(ORIG_SRC_VERSION)+dfsg
	mkdir gmp-doc-$(ORIG_SRC_VERSION)
	mv gmp-$(ORIG_SRC_VERSION)+dfsg/doc/ gmp-doc-$(ORIG_SRC_VERSION)/
	mkdir gmp-$(ORIG_SRC_VERSION)+dfsg/doc
	touch gmp-$(ORIG_SRC_VERSION)+dfsg/doc/Makefile.am
	touch gmp-$(ORIG_SRC_VERSION)+dfsg/doc/Makefile.in
	cd gmp-$(ORIG_SRC_VERSION)+dfsg/ && autoreconf --force --install
	tar --xz -cf gmp_$(ORIG_SRC_VERSION)+dfsg.orig.tar.xz gmp-$(ORIG_SRC_VERSION)+dfsg
	tar --xz -cf gmp-doc_$(ORIG_SRC_VERSION).orig.tar.xz gmp-doc-$(ORIG_SRC_VERSION)
	rm -rf gmp-$(ORIG_SRC_VERSION)+dfsg gmp-doc-$(ORIG_SRC_VERSION)

gmp-$(ORIG_SRC_VERSION).tar.xz:
	wget https://gmplib.org/download/gmp/$@

configure: configure-stamp-32 configure-stamp-64
configure-stamp-32:
	mkdir -p build32
	cd build32 && ../configure $(confflags_ma) \
	    --libdir=/usr/lib ABI=32 \
	    AR=$(AR) CC="$(CC) -m32" CFLAGS="$(CFLAGS)" \
	    CXX="$(CXX) -m32" CXXFLAGS="$(CXXFLAGS)" MPN_PATH="$(MPN_32)"
	touch $@

configure-stamp-64:
	mkdir -p build64
	cd build64 && ../configure $(confflags_ma) \
	    --libdir=/usr/lib/$(DEB_HOST_MULTIARCH) ABI=64 \
	    AR=$(AR) CC="$(CC) -m64" CFLAGS="$(CFLAGS) $(SAVEARGS)" \
	    CXX="$(CXX) -m64" CXXFLAGS="$(CXXFLAGS) $(SAVEARGS)" MPN_PATH="$(MPN_64)"
	touch $@

build: build-stamp-32 build-stamp-64
build-stamp-32: configure
	dh_testdir
	$(MAKE) $(JOBSFLAG) -C build32
	$(MAKE_CHECK) -C build32
	touch $@

build-stamp-64: configure
	dh_testdir
	$(MAKE) $(JOBSFLAG) -C build64
	$(MAKE_CHECK) -C build64
	touch $@

clean:
	dh_testdir
	dh_testroot
	rm -rf build32 build64 \
		build-stamp-32 build-stamp-64 \
		configure-stamp-32 configure-stamp-64
	dh_clean

install-prep:
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs

install: build-stamp-32 build-stamp-64 install-prep
	rm -rf debian/tmp
	# Install places gmp.h in 'includeexecdir' which is non-standard and cannot be set at compile time,
	# so override it at install.
#	$(MAKE) DESTDIR=`pwd`/debian/tmp includeexecdir=/usr/include/$(DEB_HOST_MULTIARCH) -C build install
	$(MAKE) DESTDIR=`pwd`/debian/tmp -C build32 install
	$(MAKE) DESTDIR=`pwd`/debian/tmp -C build64 install

	dh_install -plibgmp10 usr/lib/*/libgmp.so.*
	dh_install -plibgmp10 usr/lib/libgmp.so.*
	dh_install -plibgmpxx4ldbl usr/lib/*/libgmpxx.so.*
	dh_install -plibgmpxx4ldbl usr/lib/libgmpxx.so.*

	dh_install -plibgmp-dev usr/lib/*/lib*.so
	dh_install -plibgmp-dev usr/lib/*/lib*.a
	dh_install -plibgmp-dev usr/lib/lib*.so
	dh_install -plibgmp-dev usr/lib/lib*.a
	dh_install -plibgmp-dev usr/include

	# Install upstream ChangeLog, AUTHORS, NEWS, and README only in -dev package
	dh_installchangelogs -plibgmp-dev
	dh_installdocs -plibgmp-dev AUTHORS NEWS README

# This single target is used to build all the packages, all at once, or
# one at a time. So keep in mind: any options passed to commands here will
# affect _all_ packages. Anything you want to only affect one package
# should be put in another target, such as the install target.
binary-common:
	# See 633312, http://wiki.debian.org/ReleaseGoals/LAFileRemoval
	sed -i "/dependency_libs/ s/'.*'/''/" `find debian/ -name '*.la'`
	dh_testdir
	dh_testroot
	# User lower compatibility to avoid installing upstream changelog in all packages
	DH_COMPAT=6 dh_installchangelogs
	dh_installdocs
	dh_installexamples
	dh_installmenu
	dh_lintian
	dh_strip -Xlibgmpxx.so.4.5.2
	dh_link
	dh_compress
	dh_fixperms
	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

# Build architecture independant packages using the common target.
binary-indep: build install
	 $(MAKE) -f debian/rules DH_OPTIONS=-i binary-common

# Build architecture dependant packages using the common target.
binary-arch: build install $(EXTRA_INSTALL)
	$(MAKE) -f debian/rules DH_OPTIONS=-s binary-common

# Any other binary targets build just one binary package at a time.
binary-%: build install
	make -f debian/rules binary-common DH_OPTIONS=-p$*

build-arch: build
build-indep: build

binary: binary-indep binary-arch
.PHONY: build build-arch build-indep clean binary-indep binary-arch binary-common binary install install-prep
