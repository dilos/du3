libtecla (1.6.3-2.1-1+dilos2) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Mon, 04 Jan 2021 17:12:19 +0300

libtecla (1.6.3-2.1) unstable; urgency=medium

  [ Helmut Grohne ]
  * Do not strip during make install. (Closes: #901467)

  [ Andreas Tille ]
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4

 -- Andreas Tille <tille@debian.org>  Thu, 14 Jun 2018 13:14:22 +0200

libtecla (1.6.3-2) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * Standards-Version: 4.0.1 (no changes needed)
  * debhelper 10
  * Enable gl_get_line() to detect EOF (Thanks for the patch to Guido
    Berhoerster <guido+debian.org@berhoerster.name>)
    Closes: #872865
  * hardening

 -- Andreas Tille <tille@debian.org>  Tue, 22 Aug 2017 21:42:28 +0200

libtecla (1.6.3-1) unstable; urgency=medium

  * New upstream version
    Closes: #791525
  * Fix some spelling issues in manpages

 -- Andreas Tille <tille@debian.org>  Mon, 06 Jul 2015 14:56:49 +0200

libtecla (1.6.2-2) unstable; urgency=medium

  [ Peter Michael Green ]
  * use dh $@ --with autotools_dev,autoreconf so that config.sub and
    config.guess get updated in this non-automake using package. Fixes build
    on arm64.
    Closes: #762351

  [ Andreas Tille ]
  * d/control: Build-Depends: autotools-dev
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Mon, 29 Sep 2014 12:26:57 +0200

libtecla (1.6.2-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * debhelper 9
  * use dh_autoreconf to simplify kfreebsd+hurd.patch
  * debian/patches/remove_makefile_to_enable_smooth_regeneration.patch:
    Avoid recursive make call by simply removing redundant Makefile
    Closes: #760790
  * Fix d-shlibs call and use unversioned devel package
  * debian/copyright: DEP5

 -- Andreas Tille <tille@debian.org>  Mon, 08 Sep 2014 09:18:09 +0200

libtecla (1.6.1-5) unstable; urgency=low

  * debian/rules: Another patch provided by Petr Salinger
    <Petr.Salinger@seznam.cz> which hopefully finally
    Closes: #621887

 -- Andreas Tille <tille@debian.org>  Thu, 15 Dec 2011 09:05:24 +0100

libtecla (1.6.1-4) unstable; urgency=low

  * debian/patches/kfreebsd+hurd.patch:
    - Next try to fix FTBGS on kfreebsd
      Closes: #621887
    - Add "*-*-gnu*" as further case to configure.in to enable
      building on GNU/Hurd
      Closes: #651937

 -- Andreas Tille <tille@debian.org>  Wed, 14 Dec 2011 08:08:44 +0100

libtecla (1.6.1-3) unstable; urgency=low

  * Use autotools properly (and for simplicity move to short
    dh notation)
    (really) Closes: #621887

 -- Andreas Tille <tille@debian.org>  Tue, 13 Dec 2011 19:39:30 +0100

libtecla (1.6.1-2) unstable; urgency=low

  * debian/patches/kfreebsd.patch: Add kfreebsd to configure.in and
    run autoreconf -vfi afterwards.
    Closes: #621887
  * debian/control:
    - Standards-Version: 3.9.2 (no changes needed)
    - Added myself to uploaders
    - Fixed VCS fields
  * Debhelper 8 (control+compat)

 -- Andreas Tille <tille@debian.org>  Tue, 13 Dec 2011 11:00:44 +0100

libtecla (1.6.1-1) unstable; urgency=low

  * Initial Release. (closes: #612625)

 -- Scott Christley <schristley@mac.com>  Tue, 08 Feb 2011 15:05:17 -0800
