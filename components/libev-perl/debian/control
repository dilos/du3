Source: libev-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper (>= 11),
               perl,
               libcanary-stability-perl,
               libcommon-sense-perl
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libev-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libev-perl.git
Homepage: https://metacpan.org/release/EV

Package: libev-perl
Architecture: any
Depends: ${perl:Depends},
         ${misc:Depends},
         ${shlibs:Depends},
         libcommon-sense-perl
Description: Perl interface to libev, the high performance event loop
 EV provides a Perl interface to libev, a high performance and full-featured
 event loop that is loosely modelled after libevent.
 .
 It includes relative timers, absolute timers with customized rescheduling,
 synchronous signals, process status change events, event watchers dealing
 with the event loop itself, file watchers, and even limited support for
 fork events.
 .
 It uses a priority queue to manage timers and uses arrays as fundamental
 data structure. It has no artificial limitations on the number of watchers
 waiting for the same event.
