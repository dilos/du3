#!/usr/bin/make -f

#export DH_VERBOSE=1
export NV_VERBOSE=1

# see FEATURE AREAS in dpkg-buildflags(1)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export CC=gcc

include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/pkg-info.mk

DEB_HOST_ARCH?= $(shell dpkg-architecture -qDEB_HOST_ARCH)

PKG_CONFIG_XORG_XSERVER_LIBDIR	 = $(shell pkg-config --variable=libdir xorg-server)
CPPFLAGS		+= -DPKG_CONFIG_XORG_XSERVER_LIBDIR='\"$(PKG_CONFIG_XORG_XSERVER_LIBDIR)\"'

export PREFIX		 = /usr
export DO_STRIP		 =
export CC_ONLY_CFLAGS	 = $(CPPFLAGS)
# reproducible builds: instead of _out/$(uname)_($uname -m) use a fixed directory
export OUTPUTDIR	 = _out/debian

ifneq (,$(filter noopt,$(DEB_BUILD_OPTIONS)))
export DEBUG		 = 1
endif

export NV_USE_BUNDLED_LIBJANSSON = 0

include debian/rules.defs
NVIDIA_LEGACY		?=
NVIDIA_TESLA		?=
NVIDIA_RELEASE		 = $(DEB_VERSION_UPSTREAM)
variant			 = $(if $(NVIDIA_TESLA),tesla-$(NVIDIA_TESLA),$(if $(NVIDIA_LEGACY),legacy-$(NVIDIA_LEGACY)xx))
variant_description	 = $(if $(NVIDIA_TESLA), (Tesla $(NVIDIA_TESLA) version),$(if $(NVIDIA_LEGACY), ($(NVIDIA_LEGACY)xx legacy version)))
-variant		 = $(if $(variant),-$(variant))
nvidia			 = nvidia$(-variant)
nvidia_private		 = nvidia/$(if $(variant),$(variant),current)
watch_version		?= $(or $(NVIDIA_TESLA),$(NVIDIA_LEGACY))

ALL_CONTROL	:= $(wildcard debian/nvidia-settings.*)
VARIANT_CONTROL	:= $(wildcard debian/nvidia-settings$(-variant).*)
RENAME_CONTROL	 = $(filter-out $(VARIANT_CONTROL),$(ALL_CONTROL))
RENAMED_CONTROL	 = $(patsubst debian/nvidia-settings.%,debian/nvidia-settings$(-variant).%,$(RENAME_CONTROL))
TEMPLATES	:= $(wildcard debian/*.in)
AUTOGEN		+= $(patsubst %.in,%,$(TEMPLATES))
AUTOGEN		+= $(patsubst %.in,%,$(RENAMED_CONTROL))
AUTOGEN		+= debian/substvars
AUTOKEEP	 = debian/watch
AUTOCLEAN	 = $(filter-out $(AUTOKEEP),$(AUTOGEN))


%:
	dh $@

ifneq (i386,$(DEB_HOST_ARCH))
override_dh_auto_build: $(AUTOGEN)
	CC_ONLY_CFLAGS="$(CPPFLAGS)" dh_auto_build --sourcedirectory=src/libXNVCtrl

override_dh_auto_install:
else
override_dh_auto_build: $(AUTOGEN)
	CC_ONLY_CFLAGS="$(CPPFLAGS)" dh_auto_build --sourcedirectory=src/libXNVCtrl
	CC_ONLY_CFLAGS="$(CPPFLAGS)" dh_auto_build -O--parallel
	$(MAKE) -C samples

override_dh_auto_install:
	dh_auto_install --destdir=debian/tmp
	install -m 0755 samples/$(OUTPUTDIR)/nv-control-dpy debian/tmp/usr/bin/
	install -m 0644 doc/nvidia-settings.png debian/tmp/nvidia-settings$(-variant).png

override_dh_strip:
	dh_strip -Nnvidia-settings$(-variant)
	dh_strip --remaining-packages --no-automatic-dbgsym

override_dh_dwz:
	dh_strip -Nnvidia-settings$(-variant)
endif

override_dh_missing:
	dh_missing --fail-missing

override_dh_auto_clean:
	dh_auto_clean
	$(MAKE) -C samples clean
	$(RM) -r doc/_out samples/_out src/_out src/libXNVCtrl/_out
	$(RM) $(AUTOCLEAN)
	$(MAKE) -f debian/rules $(AUTOKEEP)

override_dh_gencontrol:
	set -ex; \
	if [ -f debian/nvidia-settings$(-variant).substvars ]; then \
		sed -ri -e 's/(^[^:]*:Depends=)/\1 GTK3 | GTK2, /' \
			-e 's/(GTK2)(.*)(libgtk2.0-0 \([^\)]*\))/\3\2/' \
			-e 's/(GTK3)(.*)(libgtk-3-0 \([^\)]*\))/\3\2/' \
			debian/nvidia-settings$(-variant).substvars ; \
		! grep GTK debian/nvidia-settings$(-variant).substvars ; \
	fi
	dh_gencontrol -- \
		-V'nvidia=$(nvidia)' \
		-V'nvidia:VariantDesc=$(variant_description)' \
		-V'nvidia:Version=$(NVIDIA_RELEASE)'

# used by dpkg-genchanges
debian/substvars:
	echo 'nvidia:VariantDesc=$(variant_description)' > $@


# Generating control files
debian/%:: debian/%.in debian/rules debian/rules.defs
	perl -p \
		-e 's{#VARIANT#}{$(-variant)}g;' \
		-e 's{#VARIANT_DESC#}{$(variant_description)}g;' \
		-e 's{#PRIVATE#}{$(nvidia_private)}g;' \
		-e 's{#WATCH_VERSION#}{$(watch_version)}g;' \
		< $< > $@

# Renaming control files for variant packaging
ifneq (,$(variant))
nvidia-settings$(-variant).%: nvidia-settings.%
	cp -a $< $@
endif
