#!/usr/bin/sh
#
# Copyright (c) 2012-2021, DilOS.
#
# Permission is hereby granted, free of charge, to any person obtaining a  copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the  rights
# to use, copy, modify, merge, publish,  distribute,  sublicense,  and/or  sell
# copies of the Software, and  to  permit  persons  to  whom  the  Software  is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall  be  included  in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY  KIND,  EXPRESS  OR
# IMPLIED, INCLUDING BUT NOT LIMITED  TO  THE  WARRANTIES  OF  MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT  SHALL  THE
# AUTHORS OR COPYRIGHT HOLDERS BE  LIABLE  FOR  ANY  CLAIM,  DAMAGES  OR  OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

. /lib/svc/share/smf_include.sh

SAMBA_CONFIG=/etc/samba/smb.conf

#export NSS_STRICT_NOFORK=DISABLED

case "$1" in

start)
	if [ ! -f "$SAMBA_CONFIG" ]; then
		echo "Configuration file '$SAMBA_CONFIG' does not exist."
		exit $SMF_EXIT_ERR_CONFIG
	fi

	# Command to execute is found in second and further script arguments
	shift
	eval "$@  --option=logging=file"
	;;

stop)
	# kill whole contract group

	# first send TERM signal and wait 30 seconds
	smf_kill_contract $2 TERM 1 30
	ret=$?
	[ $ret -eq 1 ] && exit $SMF_EXIT_ERR_FATAL

	# If there are still processes running, KILL them
	if [ $ret -eq 2 ] ; then
		smf_kill_contract $2 KILL 1
	fi
	;;

*)
	cat <<-EOT
		Usage:
		  $0 start <command to run>
		  $0 stop <contract number to kill>
	EOT
	exit 1
	;;
esac
