#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-
# Copyright 2015-2018 Jonas Smedegaard <dr@jones.dk>
# Description: Main Debian packaging script for sassc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

DEB_AUTO_UPDATE_LIBTOOL = pre
DEB_AUTO_UPDATE_ACLOCAL = ,
DEB_AUTO_UPDATE_AUTOCONF = ,
DEB_AUTO_UPDATE_AUTOMAKE = ,
DEB_AUTO_UPDATE_AUTOHEADER = ,
include /usr/share/cdbs/1/rules/utils.mk
include /usr/share/cdbs/1/class/autotools.mk
include /usr/share/cdbs/1/rules/debhelper.mk

pkg = $(DEB_SOURCE_PACKAGE)

# put aside upstream-shipped non-autotools Makefile during build
DEB_UPSTREAM_CRUFT_MOVE = Makefile sassc_version.h

# create autotools files from scratch during build
auxdir = script
macrodir = m4
makefiledirs = .
files_autotools_core = $(auxdir)/config.guess $(auxdir)/config.sub
stems_m4_libtool = libtool ltoptions ltsugar ltversion lt~obsolete
files_libtool = $(auxdir)/ltmain.sh $(stems_m4_libtool:%=$(macrodir)/%.m4)
stems_aux_autoconf = compile install-sh missing ar-lib
files_autoconf = aclocal.m4 configure $(stems_aux_autoconf:%=$(auxdir)/%)
files_automake = $(auxdir)/depcomp $(makefiledirs:=/Makefile.in)
#files_automake += $(auxdir)/test-driver
files_autoheader = config.h.in
files_configure = config.log
files_autotools = $(files_autotools_core) $(files_libtool)
files_autotools += $(files_aclocal) $(files_autoconf) $(files_automake)
files_autotools += $(files_autoheader) $(files_configure)
DEB_ACLOCAL_ARGS = -Im4 --install --force
DEB_AUTOMAKE_ARGS = --add-missing --copy --foreign --force
DEB_MAKE_CLEAN_TARGET = distclean
makefile-clean::
#	rm -rf autom4te.cache
	rm -f $(filter-out $(DEB_UPSTREAM_CRUFT_MOVE),$(files_autotools))
clean::
	rm -f $(filter-out $(DEB_UPSTREAM_CRUFT_MOVE),$(files_autotools_core))
	test ! -d $(auxdir) || rmdir --ignore-fail-on-non-empty $(auxdir)

# generate manpages based on --help of script itself
manpages = $(patsubst %,debian/%.1,sassc)
DEB_INSTALL_MANPAGES_$(pkg) = $(manpages)
common-build-arch:: $(manpages)
$(manpages): debian/%.1 : % build/$(pkg)
	help2man \
		--name "Sass compiles CSS from SASS or SCSS files" \
		--no-info --output=$@ ./$< \
		|| { ./$< --help; false; }
makefile-clean::
	rm -f $(manpages)

pre-build:: debian/stamp-set-version
debian/stamp-set-version: debian/stamp-upstream-cruft
	echo $(DEB_UPSTREAM_VERSION) > VERSION
	touch $@
makefile-clean::
	rm -f VERSION debian/stamp-set-version

# copy unusually name Makefile.am to help CDBS notice it
debian/stamp-autotools-files: Makefile.am
Makefile.am:
	cp -f GNUmakefile.am Makefile.am
clean::
	rm -f Makefile.am
