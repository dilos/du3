#!/bin/sh
set -e

elfname="run_tests"

# compile src into object files
srcs=$(find test/cctest -type f -name '*.cc' -print)
for src in $srcs; do
	if ! test -r ${src%.cc}.o; then
		g++ -march=native -c $src -o ${src%.cc}.o
		echo CXX $src
	fi
done

# link
objs=$(find test/cctest -type f -name '*.o' -print)
echo $objs | xargs g++ -o $elfname -ldouble-conversion
echo LD $elfname

# execute

./$elfname --list | sed -e 's/<$//' | xargs ./$elfname

# cleanup

rm test/cctest/*.o
rm run_tests
