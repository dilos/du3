#!/usr/bin/perl
# Debconf configuration script for PADL-ldap tools.
# By Sami Haahtinen <ressu@debian.org>

$conffile="/etc/pam_ldap.conf";
$action=shift;
$from_version=shift;

use Debconf::Client::ConfModule ':all';
version('2.0');

my @ret;
my @current_config;

# The 'override' thing really ought to go, but let's see how this works
# out first.

if(-e $conffile) {
	open CONFIG, "<$conffile";
	if(<CONFIG> =~ /^###DEBCONF###$/) {
		set("libpam-ldap/override", "true");
	} else {
		my $oldval=get("libpam-ldap/override");
		set("libpam-ldap/override", "false"); 
		if ($oldval eq "true") {
			fset("libpam-ldap/override", "seen", "false")
		}

		# whee.. the same deal as with libnss-ldap, critical
		# priority with reconfigure otherwise it's just high
		
		input($action =~ /reconfigure/ ? "critical" : "high", 
			"libpam-ldap/override");
		$ret=go();
	};
	@current_config = <CONFIG>;
	close CONFIG;
} else { 
	set("libpam-ldap/override", "true");
};

# filename/package name substitutions; we keep these out of the
# text of the templates so that more translations can be directly shared
# between libpam-ldap and libnss-ldap.
subst('libpam-ldap/rootbindpw','filename','/etc/pam_ldap.secret');
subst('libpam-ldap/rootbindpw','package','libpam-ldap');

if(get("libpam-ldap/override") eq "true") {
	# don't forget to check for any values of 'host' here --
	# it may be better to just prepend 'ldap://' and migrate
	# these all to URI so we can deprecate HOST, but for the time
	# being this should adequately address our needs
	my $value = (grep(/^host\s/, @current_config))[0];
	if ($value) {
		chomp($value);
		$value =~ s/^host\s+//;
		set('shared/ldapns/ldap-server', $value);
	}
	# These are the same as with libnss-ldap, lets not touch those.
	read_and_input('shared/ldapns/ldap-server', 'uri', 'critical');
	read_and_input('shared/ldapns/base-dn', 'base', 'critical');
	read_and_input('shared/ldapns/ldap_version', 'ldap_version', 'critical');
	$ret = go();  # yeah, we don't need that.. but in case we sometime do

	# dbrootlogin will most likely break.. i need to deal with it
	# someday..
	input("high", "libpam-ldap/dbrootlogin");
	input("high", "libpam-ldap/dblogin");
	$ret = go();

	if(get("libpam-ldap/dbrootlogin") eq "true") {
		read_and_input('libpam-ldap/rootbinddn', 'rootbinddn', 'critical');
		input('critical', 'libpam-ldap/rootbindpw');
		$ret = go()
	}

	if(get("libpam-ldap/dblogin") eq "true") {
		# user wants to login.. 
		read_and_input('libpam-ldap/binddn', 'binddn', 'critical');
		read_and_input('libpam-ldap/bindpw', 'bindpw', 'critical');
		$ret = go();
	}
	read_and_input('libpam-ldap/pam_password', 'pam_password', 'medium');
	$ret = go();
}


sub read_and_input
{
	my ($debconf_name, $conffile_name, $priority) = @_;
	$priority = 'medium' unless $priority;

	my @valuelist = grep(/^$conffile_name\s/, @current_config);
	if (@valuelist) {
		my $value = pop(@valuelist);
		chomp($value);
		$value =~ s/^$conffile_name\s+//;
		set($debconf_name, $value);
	}
	input($priority, $debconf_name);
}
