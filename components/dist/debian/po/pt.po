# Portuguese translation of dist's debconf messages.
# Copyright (C) 2007
# This file is distributed under the same license as the dist package.
# Luísa Lourenço <kikentai@gmail.com>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: dist 3.70-31\n"
"Report-Msgid-Bugs-To: dist@packages.debian.org\n"
"POT-Creation-Date: 2012-02-20 07:13+0100\n"
"PO-Revision-Date: 2007-03-28 11:04+0100\n"
"Last-Translator: Luísa Lourenço <kikentai@gmail.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Name of your organization:"
msgstr "Nome da sua organização:"

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"You don't seem to have an /etc/news/organization file. Usually that contains "
"the name of your organization as you want it to appear on the Organization "
"line of outgoing articles/mail/patches. Please supply the name of your "
"organization as you want it to appear on the Organization line of outgoing "
"articles/patches.  (It is nice if this also specifies your location.  Your "
"city name is probably sufficient if well known.) For example:"
msgstr ""
"Não parece ter um ficheiro /etc/news/organization. Normalmente contém o nome "
"da sua organização tal como quer que apareça na linha Organização de artigos/"
"correio/patches. Por favor forneça o nome da sua organização tal como quer "
"que apareça na linha Organização de artigos/correio/patches.  (Seria "
"simpático se isto também especificar a sua localização. O nome da sua cidade "
"é provavelmente suficiente se for conhecido.) Por exemplo:"

#. Type: string
#. Description
#: ../templates:1001
msgid "   University of Southern North Dakota, Hoople"
msgstr "   Universidade Nova de Lisboa, Monte da Caparica"

#. Type: string
#. Description
#: ../templates:1001
msgid "Type in \"--none--\" if you do not want to specify one."
msgstr "Escreva \"--nenhum--\" caso não queira especificar um nome."
