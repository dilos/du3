adjust built-in search paths for kpathsea library to Debian standard
---
 texk/kpathsea/texmf.cnf |   50 +++++++++++++-----------------------------------
 1 file changed, 14 insertions(+), 36 deletions(-)

--- texlive-bin.orig/texk/kpathsea/texmf.cnf
+++ texlive-bin/texk/kpathsea/texmf.cnf
@@ -61,20 +61,23 @@
 TEXMFROOT = $SELFAUTOPARENT
 
 % The main tree of distributed packages and programs:
-TEXMFDIST = $TEXMFROOT/texmf-dist
+TEXMFDIST = /usr/share/texlive/texmf-dist
 
 % We used to have a separate /texmf tree with some core programs and files.
 % Keep the variable name.
 TEXMFMAIN = $TEXMFDIST
 
+% The Debian search tree
+TEXMFDEBIAN = /usr/share/texmf
+
 % Local additions to the distribution trees.
-TEXMFLOCAL = $SELFAUTOGRANDPARENT/texmf-local
+TEXMFLOCAL = /usr/local/share/texmf
 
 % TEXMFSYSVAR, where *-sys store cached runtime data.
-TEXMFSYSVAR = $TEXMFROOT/texmf-var
+TEXMFSYSVAR = /var/lib/texmf
 
 % TEXMFSYSCONFIG, where *-sys store configuration data.
-TEXMFSYSCONFIG = $TEXMFROOT/texmf-config
+TEXMFSYSCONFIG = /etc/texmf
 
 % Per-user texmf tree(s) -- organized per the TDS, as usual.  To define
 % more than one per-user tree, set this to a list of directories in
@@ -83,10 +86,10 @@ TEXMFSYSCONFIG = $TEXMFROOT/texmf-config
 TEXMFHOME = ~/texmf
 
 % TEXMFVAR, where texconfig/updmap/fmtutil store cached runtime data.
-TEXMFVAR = ~/.texlive2018/texmf-var
+TEXMFVAR = ~/.texmf-var
 
 % TEXMFCONFIG, where texconfig/updmap/fmtutil store configuration data.
-TEXMFCONFIG = ~/.texlive2018/texmf-config
+TEXMFCONFIG = ~/.texmf-config
 
 % This is the value manipulated by tlmgr's auxtrees subcommand in the
 % root texmf.cnf. Kpathsea warns about a literally empty string for a
@@ -107,7 +110,7 @@ TEXMFAUXTREES = {}
 % The odd-looking $TEXMFAUXTREES$TEXMF... construct is so that if no auxtree is
 % ever defined (the 99% common case), no extra elements will be added to
 % the search paths. tlmgr takes care to end any value with a trailing comma.
-TEXMF = {$TEXMFAUXTREES$TEXMFCONFIG,$TEXMFVAR,$TEXMFHOME,!!$TEXMFLOCAL,!!$TEXMFSYSCONFIG,!!$TEXMFSYSVAR,!!$TEXMFDIST}
+TEXMF = {$TEXMFAUXTREES$TEXMFCONFIG,$TEXMFVAR,$TEXMFHOME,!!$TEXMFLOCAL,!!$TEXMFSYSCONFIG,!!$TEXMFSYSVAR,!!$TEXMFDEBIAN,!!$TEXMFDIST}
 
 % Where to look for ls-R files.  There need not be an ls-R in the
 % directories in this path, but if there is one, Kpathsea will use it.
@@ -115,7 +118,7 @@ TEXMF = {$TEXMFAUXTREES$TEXMFCONFIG,$TEX
 % does not create ls-R files in the non-!! elements -- because if an
 % ls-R is present, it will be used, and the disk will not be searched.
 % This is arguably a bug in kpathsea.
-TEXMFDBS = {!!$TEXMFLOCAL,!!$TEXMFSYSCONFIG,!!$TEXMFSYSVAR,!!$TEXMFDIST}
+TEXMFDBS = {!!$TEXMFLOCAL,!!$TEXMFSYSCONFIG,!!$TEXMFSYSVAR,!!$TEXMFDEBIAN,!!$TEXMFDIST}
 
 % The system trees.  These are the trees that are shared by all users.
 % If a tree appears in this list, the mktex* scripts will use
@@ -132,7 +135,7 @@ TEXMFCACHE = $TEXMFSYSVAR;$TEXMFVAR
 % Where generated fonts may be written.  This tree is used when the sources
 % were found in a system tree and either that tree wasn't writable, or the
 % varfonts feature was enabled in MT_FEATURES in mktex.cnf.
-VARTEXFONTS = $TEXMFVAR/fonts
+VARTEXFONTS = /var/cache/fonts
 
 % On some systems, there will be a system tree which contains all the font
 % files that may be created as well as the formats.  For example
@@ -520,33 +523,8 @@ RUBYINPUTS   = $TEXMFDOTDIR;$TEXMF/scrip
 % since we don't want to scatter ../'s throughout the value.  Hence we
 % explicitly list every directory.  Arguably more understandable anyway.
 %
-TEXMFCNF = {\
-$SELFAUTOLOC,\
-$SELFAUTOLOC/share/texmf-local/web2c,\
-$SELFAUTOLOC/share/texmf-dist/web2c,\
-$SELFAUTOLOC/share/texmf/web2c,\
-$SELFAUTOLOC/texmf-local/web2c,\
-$SELFAUTOLOC/texmf-dist/web2c,\
-$SELFAUTOLOC/texmf/web2c,\
-\
-$SELFAUTODIR,\
-$SELFAUTODIR/share/texmf-local/web2c,\
-$SELFAUTODIR/share/texmf-dist/web2c,\
-$SELFAUTODIR/share/texmf/web2c,\
-$SELFAUTODIR/texmf-local/web2c,\
-$SELFAUTODIR/texmf-dist/web2c,\
-$SELFAUTODIR/texmf/web2c,\
-\
-$SELFAUTOGRANDPARENT/texmf-local/web2c,\
-$SELFAUTOPARENT,\
-\
-$SELFAUTOPARENT/share/texmf-local/web2c,\
-$SELFAUTOPARENT/share/texmf-dist/web2c,\
-$SELFAUTOPARENT/share/texmf/web2c,\
-$SELFAUTOPARENT/texmf-local/web2c,\
-$SELFAUTOPARENT/texmf-dist/web2c,\
-$SELFAUTOPARENT/texmf/web2c\
-}
+TEXMFCNF = {/etc/texmf/web2c,/usr/local/share/texmf/web2c,/usr/share/texmf/web2c,/usr/share/texlive/texmf-dist/web2c,$SELFAUTOPARENT/share/texmf/web2c}
+
 %
 % For reference, here is the old brace-using definition:
 %TEXMFCNF = {$SELFAUTOLOC,$SELFAUTODIR,$SELFAUTOPARENT}{,{/share,}/texmf{-local,}/web2c}
