Source: ipywidgets
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Gordon Ball <gordon@chronitis.net>, Ximin Luo <infinity0@debian.org>
Section: python
Priority: optional
Standards-Version: 4.0.1
Homepage: https://github.com/jupyter-widgets/ipywidgets
Build-Depends: debhelper (>= 10),
               dh-python,
               node-recast,
               libjs-jquery,
               libjs-jquery-ui,
               node-backbone,
               node-d3-format,
               node-es6-promise,
               node-semver,
               node-typescript,
               node-typescript-types,
               node-underscore,
               patch,
               python-all,
               python-pytest,
               python-setuptools (>= 28),
               python-coverage,
               python-ipython,
               python-ipykernel,
               python-jsonschema,
               python-mock,
               python-nose,
               python-traitlets,
               python-sphinx,
               python-sphinx-rtd-theme,
               python3-all,
               python3-pytest,
               python3-setuptools (>= 28),
               python3-coverage,
               python3-ipython,
               python3-ipykernel,
               python3-jsonschema,
               python3-mock,
               python3-nose,
               python3-traitlets,
               python3-sphinx,
               python3-sphinx-rtd-theme,
               pandoc [fixme],
               m4
Vcs-Git: https://salsa.debian.org/python-team/modules/ipywidgets.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/ipywidgets

Package: python-ipywidgets
Architecture: all
Depends: ${misc:Depends}, ${python:Depends},
 jupyter-nbextension-jupyter-js-widgets (= ${binary:Version})
Suggests: python-ipywidgets-doc
Description: Interactive widgets for the Jupyter notebook (Python 2)
 Notebooks come alive when interactive widgets are used. Learning becomes an
 immersive and fun experience. Researchers can easily see how changing inputs
 to a model impact the results.
 .
 This package installs the library for Python 2 notebooks.

Package: python3-ipywidgets
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends},
 jupyter-nbextension-jupyter-js-widgets (= ${binary:Version})
Suggests: python-ipywidgets-doc
Description: Interactive widgets for the Jupyter notebook (Python 3)
 Notebooks come alive when interactive widgets are used. Learning becomes an
 immersive and fun experience. Researchers can easily see how changing inputs
 to a model impact the results.
 .
 This package installs the library for Python 3 notebooks.

Package: python-ipywidgets-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}, libjs-mathjax
Built-Using: ${fakewebpack:Built-Using}
Description: Interactive widgets for the Jupyter notebook (documentation)
 Notebooks come alive when interactive widgets are used. Learning becomes an
 immersive and fun experience. Researchers can easily see how changing inputs
 to a model impact the results.
 .
 This package installs documentation.
 .
 NOTE: currently this package is empty because upstream does not have a good
 build-process to generate the documentation; see:
 .
  * https://github.com/jupyter-widgets/ipywidgets/issues/1273
  * https://github.com/jupyter-widgets/jupyter-sphinx/issues/4

Package: jupyter-nbextension-jupyter-js-widgets
Architecture: all
# see TODO, we can eventually turn these python- Depends: into a Recommends:
Depends: ${misc:Depends}, python3-notebook (>= 4.2.3-3) | python-notebook (>= 4.2.3-3), python3 | python
Suggests: python-ipywidgets, python3-ipywidgets
Built-Using: ${fakewebpack:Built-Using}
Description: Interactive widgets - Jupyter notebook extension
 Notebooks come alive when interactive widgets are used. Learning becomes an
 immersive and fun experience. Researchers can easily see how changing inputs
 to a model impact the results.
 .
 This package contains the server-side Jupyter notebook extension that allows a
 Jupyter notebook server to serve and display these widgets. You will also need
 to install kernel-side libraries, for an interactive session to drive and
 animate the widgets that are contained in a notebook. For Python 2 notebooks
 this is python-ipywidgets, and for Python 3 this is python3-ipywidgets.

Package: python-widgetsnbextension
Architecture: all
Depends: ${misc:Depends}, ${python:Depends},
 jupyter-nbextension-jupyter-js-widgets (= ${binary:Version})
Enhances: python-notebook
Description: Interactive widgets - Jupyter notebook extension (Python 2)
 Notebooks come alive when interactive widgets are used. Learning becomes an
 immersive and fun experience. Researchers can easily see how changing inputs
 to a model impact the results.
 .
 This package contains the server-side Jupyter notebook extension as a Python 2
 module that may be installed via the notebook.nbextensions executable module.
 .
 This package should *not be necessary* for Debian software to use; instead you
 should directly use the jupyter-nbextension-jupyter-js-widgets package. It is
 provided only for compatibility with non-Debian software.

Package: python3-widgetsnbextension
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends},
 jupyter-nbextension-jupyter-js-widgets (= ${binary:Version})
Enhances: python3-notebook
Description: Interactive widgets - Jupyter notebook extension (Python 3)
 Notebooks come alive when interactive widgets are used. Learning becomes an
 immersive and fun experience. Researchers can easily see how changing inputs
 to a model impact the results.
 .
 This package contains the server-side Jupyter notebook extension as a Python 3
 module that may be installed via the notebook.nbextensions executable module.
 .
 This package should *not be necessary* for Debian software to use; instead you
 should directly use the jupyter-nbextension-jupyter-js-widgets package. It is
 provided only for compatibility with non-Debian software.
