statsmodels for Debian
----------------------

For greater flexibility and ease to cherry pick fixes, packaging
repository is a clone of upstream GIT repository with a debian/ branch
to carry the packaging. orig.tar.gz is fetched from tags on github.

 -- Yaroslav Halchenko <debian@onerussian.com>, Thu,  7 Jul 2011 22:50:02 -0400


To build the documentation without accessing the internet at Build time
several data sets are provided as cached data in the directory debian/datasets.
The files are compressed versions of text data (csv, rst) and probably not
copyrighteable.  For the sake of clarity we are using here the form of
documentation that ftpmaster suggested in
  http://lists.debian.org/debian-devel/2013/09/msg00332.html
for R packages since all data are obtained from CRAN.

Arthritis
  https://vincentarelbundock.github.io/Rdatasets/doc/vcd/Arthritis.html
  From package: vcd
  https://cran.r-project.org/web/packages/vcd/vcd.pdf
  License: GPL-2
  Source
  
  Michael Friendly (2000), Visualizing Categorical Data: http://euclid.psych.yorku.ca/ftp/sas/vcd/catdata/arthrit.sas
  References
  
  G. Koch \& S. Edwards (1988), Clinical efficiency trials with categorical data. In K. E. Peace (ed.), Biopharmaceutical Statistics for Drug Development, 403–451. Marcel Dekker, New York.
  
  M. Friendly (2000), Visualizing Categorical Data. SAS Institute, Cary, NC.
  
dietox
  https://vincentarelbundock.github.io/Rdatasets/doc/geepack/dietox.html
  From package: geepack
  https://cran.r-project.org/web/packages/geepack/geepack.pdf
  License GPL (>= 3)
  Source
  
  Lauridsen, C., Højsgaard, S.,Sørensen, M.T. C. (1999) Influence of
  Dietary Rapeseed Oli, Vitamin E, and Copper on Performance and
  Antioxidant and Oxidative Status of Pigs. J. Anim. Sci.77:906-916

Duncan
  https://vincentarelbundock.github.io/Rdatasets/doc/car/Duncan.html
  From package: car
  https://cran.r-project.org/web/packages/car/car.pdf
  License: GPL (>= 2)
  Source
  
  Duncan, O. D. (1961) A socioeconomic index for all occupations. In
  Reiss, A. J., Jr. (Ed.) Occupations and Social Status. Free Press
  [Table VI-1].
  
  References
  
  Fox, J. (2008) Applied Regression Analysis and Generalized Linear Models, Second Edition. Sage.
  
  Fox, J. and Weisberg, S. (2011) An R Companion to Applied Regression, Second Edition, Sage.

epil
  https://vincentarelbundock.github.io/Rdatasets/doc/MASS/epil.html
  From package: MASS
  https://cran.r-project.org/web/packages/MASS/MASS.pdf
  License: GPL-2 | GPL-3
  Source
  
  Thall, P. F. and Vail, S. C. (1990) Some covariance models for longitudinal count data with over-dispersion. Biometrics 46, 657–671.
  References
  
  Venables, W. N. and Ripley, B. D. (2002) Modern Applied Statistics with S. Fourth Edition. Springer. 

Guerry
  https://vincentarelbundock.github.io/Rdatasets/doc/HistData/Guerry.html
  Originally from package: HistData
  https://cran.r-project.org/web/packages/HistData/HistData.pdf
  This says GPL 2 | GPL 3
  Source
  
  Angeville, A. (1836). Essai sur la Statistique de la Population française Paris: F. Doufour.
  
  Guerry, A.-M. (1833). Essai sur la statistique morale de la France
  Paris: Crochard. English translation: Hugh P. Whitt and Victor
  W. Reinking, Lewiston, N.Y. : Edwin Mellen Press, 2002.
  
  Parent-Duchatelet, A. (1836). De la prostitution dans la ville de Paris, 3rd ed, 1857, p. 32, 36
  References
  
  Dray, S. and Jombart, T. (2011). A Revisit Of Guerry's Data:
  Introducing Spatial Constraints In Multivariate Analysis. The Annals
  of Applied Statistics, Vol. 5, No. 4,
  2278-2299. http://arxiv.org/pdf/1202.6485.pdf, DOI:
  10.1214/10-AOAS356.
  
  Brunsdon, C. and Dykes, J. (2007). Geographically weighted
  visualization: interactive graphics for scale-varying exploratory
  analysis. Geographical Information Science Research Conference
  (GISRUK 07), NUI Maynooth, Ireland, April, 2007.
  
  Friendly, M. (2007). A.-M. Guerry's Moral Statistics of France:
  Challenges for Multivariable Spatial Analysis. Statistical Science,
  22, 368-399.

  Friendly, M. (2007). Data from A.-M. Guerry, Essay on the Moral
  Statistics of France (1833),
  http://datavis.ca/gallery/guerry/guerrydat.html.

medpar
  https://vincentarelbundock.github.io/Rdatasets/doc/COUNT/medpar.html
  From package: COUNT
  https://cran.r-project.org/web/packages/COUNT/COUNT.pdf
  License: GPL-2

  Source
  
  1991 National Medpar data, National Health Economics & Research Co.
  References
  
  Hilbe, Joseph M (2014), Modeling Count Data, Cambridge University
  Press Hilbe, Joseph M (2007, 2011), Negative Binomial Regression,
  Cambridge University Press Hilbe, Joseph M (2009), Logistic
  Regression Models, Chapman & Hall/CRC first used in Hardin, JW and
  JM Hilbe (2001, 2007), Generalized Linear Models and Extensions,
  Stata Press

Moore
  https://vincentarelbundock.github.io/Rdatasets/doc/car/Moore.html
  From package: car
  https://cran.r-project.org/web/packages/car/car.pdf
  License: GPL (>= 2)
  Source
  
  Moore, J. C., Jr. and Krupat, E. (1971) Relationship between source
  status, authoritarianism and conformity in a social
  setting. Sociometry 34, 122–134.
  
  Personal communication from J. Moore, Department of Sociology, York University.
  References
  
  Fox, J. (2008) Applied Regression Analysis and Generalized Linear Models, Second Edition. Sage.
  
  Fox, J. and Weisberg, S. (2011) An R Companion to Applied Regression, Second Edition, Sage.

starsCYG
  https://vincentarelbundock.github.io/Rdatasets/doc/robustbase/starsCYG.html
  From package: robustbase
  https://cran.r-project.org/web/packages/robustbase/robustbase.pdf
  License GPL >= 2
  Source
  
  P. J. Rousseeuw and A. M. Leroy (1987) Robust Regression and Outlier Detection; Wiley, p.27, table 3. 

 -- Diane Trout <diane@ghic.org>  Tue, 26 Sep 2017 23:55:47 -0700
