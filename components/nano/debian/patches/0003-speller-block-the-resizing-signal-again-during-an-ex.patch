From 28ba36c2dcebd0a4caef1c231673c86f372c0042 Mon Sep 17 00:00:00 2001
From: Benno Schulenberg <bensberg@telfort.nl>
Date: Wed, 27 Mar 2019 17:15:31 +0100
Subject: [PATCH 3/8] speller: block the resizing signal again during an
 external spell check

Somehow a SIGWINCH pushes nano past the wait() in do_alt_speller(),
even though the external spelling program hasn't finished.

This fixes https://savannah.gnu.org/bugs/?56010
by reverting commit 1f39f60b.

Bug existed since version 3.2.
---
 src/nano.c  | 11 +++++++++++
 src/proto.h |  1 +
 src/text.c  | 10 ++++++++++
 3 files changed, 22 insertions(+)

diff --git a/src/nano.c b/src/nano.c
index 9825d457..ce5334ab 100644
--- a/src/nano.c
+++ b/src/nano.c
@@ -1359,6 +1359,17 @@ void regenerate_screen(void)
 	total_refresh();
 }
 
+/* If allow is FALSE, block any SIGWINCH signal.  If allow is TRUE,
+ * unblock SIGWINCH so any pending ones can be dealt with. */
+void allow_sigwinch(bool allow)
+{
+	sigset_t winch;
+
+	sigemptyset(&winch);
+	sigaddset(&winch, SIGWINCH);
+	sigprocmask(allow ? SIG_UNBLOCK : SIG_BLOCK, &winch, NULL);
+}
+
 /* Handle the global toggle specified in flag. */
 void do_toggle(int flag)
 {
diff --git a/src/proto.h b/src/proto.h
index b6c50e58..ccf68c45 100644
--- a/src/proto.h
+++ b/src/proto.h
@@ -424,6 +424,7 @@ RETSIGTYPE do_continue(int signal);
 #ifndef NANO_TINY
 RETSIGTYPE handle_sigwinch(int signal);
 void regenerate_screen(void);
+void allow_sigwinch(bool allow);
 void do_toggle(int flag);
 void enable_signals(void);
 #endif
diff --git a/src/text.c b/src/text.c
index 70953379..7e1b0b4c 100644
--- a/src/text.c
+++ b/src/text.c
@@ -2883,6 +2883,11 @@ const char *do_alt_speller(char *tempfile_name)
 	} else if (pid_spell < 0)
 		return _("Could not fork");
 
+#ifndef NANO_TINY
+	/* Block SIGWINCHes so the spell checker doesn't get any. */
+	allow_sigwinch(FALSE);
+#endif
+
 	/* Wait for the alternate spell checker to finish. */
 	wait(&alt_spell_status);
 
@@ -2937,6 +2942,11 @@ const char *do_alt_speller(char *tempfile_name)
 		adjust_viewport(STATIONARY);
 	}
 
+#ifndef NANO_TINY
+	/* Unblock SIGWINCHes again. */
+	allow_sigwinch(TRUE);
+#endif
+
 	return NULL;
 }
 
-- 
2.20.1

