Description: Fix a spelling error found by lintian
Origin: vendor
Author: Alessandro Ghedini <al3xbio@gmail.com>
Reviewed-by: Nicholas Bamber <nicholas@periapt.co.uk>
Last-Update: 2017-09-12
Forwarded: mailto:anyevent@lists.schmorp.de (not latest version)

--- a/lib/AnyEvent.pm
+++ b/lib/AnyEvent.pm
@@ -434,7 +434,7 @@
 C libraries for this. AnyEvent will try to do its best, which
 means in some cases, signals will be delayed. The maximum time
 a signal might be delayed is 10 seconds by default, but can
-be overriden via C<$ENV{PERL_ANYEVENT_MAX_SIGNAL_LATENCY}> or
+be overridden via C<$ENV{PERL_ANYEVENT_MAX_SIGNAL_LATENCY}> or
 C<$AnyEvent::MAX_SIGNAL_LATENCY> - see the L<ENVIRONMENT VARIABLES>
 section for details.
 
@@ -2107,7 +2107,7 @@
 The exact algorithm is currently:
 
    1. if taint mode enabled, delete all PERL_ANYEVENT_xyz variables from %ENV
-   2. copy over AE_xyz to PERL_ANYEVENT_xyz unless the latter alraedy exists
+   2. copy over AE_xyz to PERL_ANYEVENT_xyz unless the latter already exists
    3. if taint mode enabled, set all PERL_ANYEVENT_xyz variables to undef.
 
 This ensures that child processes will not see the C<AE_> variables.
@@ -2178,7 +2178,7 @@
 
 If this env variable is nonempty, then its contents will be interpreted by
 C<AnyEvent::Socket::parse_hostport> and C<AnyEvent::Debug::shell> (after
-replacing every occurance of C<$$> by the process pid). The shell object
+replacing every occurrence of C<$$> by the process pid). The shell object
 is saved in C<$AnyEvent::Debug::SHELL>.
 
 This happens when the first watcher is created.
@@ -3014,7 +3014,7 @@
 implement some kind of parallel processing is almost certainly doomed.
 
 To safely fork and exec, you should use a module such as
-L<Proc::FastSpawn> that let's you safely fork and exec new processes.
+L<Proc::FastSpawn> that lets you safely fork and exec new processes.
 
 If you want to do multiprocessing using processes, you can
 look at the L<AnyEvent::Fork> module (and some related modules
@@ -3055,7 +3055,7 @@
 
 Perl 5.8 has numerous memleaks that sometimes hit this module and are hard
 to work around. If you suffer from memleaks, first upgrade to Perl 5.10
-and check wether the leaks still show up. (Perl 5.10.0 has other annoying
+and check whether the leaks still show up. (Perl 5.10.0 has other annoying
 memleaks, such as leaking on C<map> and C<grep> but it is usually not as
 pronounced).
 
--- a/lib/AnyEvent/Handle.pm
+++ b/lib/AnyEvent/Handle.pm
@@ -159,7 +159,7 @@
 =item on_error => $cb->($handle, $fatal, $message)
 
 This is the error callback, which is called when, well, some error
-occured, such as not being able to resolve the hostname, failure to
+occurred, such as not being able to resolve the hostname, failure to
 connect, or a read error.
 
 Some errors are fatal (which is indicated by C<$fatal> being true). On
@@ -367,7 +367,7 @@
 security implications, AnyEvent::Handle sets this flag automatically
 unless explicitly specified. Note that setting this flag after
 establishing a connection I<may> be a bit too late (data loss could
-already have occured on BSD systems), but at least it will protect you
+already have occurred on BSD systems), but at least it will protect you
 from most attacks.
 
 =item read_size => <bytes>
@@ -517,7 +517,7 @@
 
 For this reason, if the default encoder uses L<JSON::XS>, it will default
 to not allowing anything but arrays and objects/hashes, at least for the
-forseeable future (it will change at some point). This might or might not
+foreseeable future (it will change at some point). This might or might not
 be true for the L<JSON> module, so this might cause a security issue.
 
 If you depend on either behaviour, you should create your own json object
@@ -1875,7 +1875,7 @@
 dtection, make sure that any non-TLS data doesn't start with the octet 22
 (ASCII SYN, 16 hex) or 128-255 (i.e. highest bit set). The checks this
 read type does are a bit more strict, but might losen in the future to
-accomodate protocol changes.
+accommodate protocol changes.
 
 This read type does not rely on L<AnyEvent::TLS> (and thus, not on
 L<Net::SSLeay>).
--- a/lib/AnyEvent/TLS.pm
+++ b/lib/AnyEvent/TLS.pm
@@ -426,7 +426,7 @@
 
   certificate private key
   client/server certificate
-  ca 1, signing client/server certficate
+  ca 1, signing client/server certificate
   ca 2, signing ca 1
   ...
 
--- a/lib/AnyEvent/Intro.pod
+++ b/lib/AnyEvent/Intro.pod
@@ -60,7 +60,7 @@
 The motivation behind these designs is often that a module doesn't want
 to depend on some complicated XS-module (Net::IRC), or that it doesn't
 want to force the user to use some specific event loop at all (LWP), out
-of fear of severly limiting the usefulness of the module: If your module
+of fear of severely limiting the usefulness of the module: If your module
 requires Glib, it will not run in a Tk program.
 
 L<AnyEvent> solves this dilemma, by B<not> forcing module authors to
@@ -573,7 +573,7 @@
                my $len = sysread $fh, $response, 1024, length $response;
 
                if ($len <= 0) {
-                  # we are done, or an error occured, lets ignore the latter
+                  # we are done, or an error occurred, lets ignore the latter
                   undef $read_watcher; # no longer interested
                   $cv->send ($response); # send results
                }
@@ -1193,7 +1193,7 @@
          });
 
       } else {
-         # some error occured, no article data
+         # some error occurred, no article data
          
          $finish->($status);
       }
--- a/lib/AnyEvent/DNS.pm
+++ b/lib/AnyEvent/DNS.pm
@@ -1268,7 +1268,7 @@
 
 This is the main low-level workhorse for sending DNS requests.
 
-This function sends a single request (a hash-ref formated as specified
+This function sends a single request (a hash-ref formatted as specified
 for C<dns_pack>) to the configured nameservers in turn until it gets a
 response. It handles timeouts, retries and automatically falls back to
 virtual circuit mode (TCP) when it receives a truncated reply. It does not
--- a/lib/AnyEvent/Debug.pm
+++ b/lib/AnyEvent/Debug.pm
@@ -68,7 +68,7 @@
 variables still in the global scope means you can debug them easier.
 
 As no authentication is done, in most cases it is best not to use a TCP
-port, but a unix domain socket, whcih can be put wherever you can access
+port, but a unix domain socket, which can be put wherever you can access
 it, but not others:
 
    our $SHELL = AnyEvent::Debug::shell "unix/", "/home/schmorp/shell";
@@ -84,7 +84,7 @@
 
    socat readline,history=.anyevent-history unix:shell
 
-Binding on C<127.0.0.1> (or C<::1>) might be a less secure but sitll not
+Binding on C<127.0.0.1> (or C<::1>) might be a less secure but still not
 totally insecure (on single-user machines) alternative to let you use
 other tools, such as telnet:
 
@@ -583,7 +583,7 @@
 
 Each object is a relatively standard hash with the following members:
 
-   type   => name of the method used ot create the watcher (e.g. C<io>, C<timer>).
+   type   => name of the method used to create the watcher (e.g. C<io>, C<timer>).
    w      => the actual watcher
    rfile  => reference to the filename of the file the watcher was created in
    line   => line number where it was created
--- a/lib/AnyEvent/Log.pm
+++ b/lib/AnyEvent/Log.pm
@@ -1230,7 +1230,7 @@
 name is first mentioned. The difference to package contexts is that by
 default they have no attached slaves.
 
-This makes it possible to create new log contexts that can be refered to
+This makes it possible to create new log contexts that can be referred to
 multiple times by name within the same log specification.
 
 =item a perl package name
--- a/lib/AnyEvent/Impl/IOAsync.pm
+++ b/lib/AnyEvent/Impl/IOAsync.pm
@@ -38,7 +38,7 @@
 choice.
 
 Note that switching loops while watchers are already initialised can have
-unexpected effects, and is not supported unless you can live witht he
+unexpected effects, and is not supported unless you can live with the
 consequences.
 
 =item $AnyEvent::Impl::IOAsync::LOOP
