From baeb17eb76772a2dbbcc4d52a22ddfc405276db1 Mon Sep 17 00:00:00 2001
From: Alex Wilson <alex.wilson@joyent.com>
Date: Fri, 4 Sep 2015 11:04:30 -0700
Subject: [PATCH 29/34] Accept LANG and LC_* environment variables from clients
 by default

This preserves most of the old SunSSH locale negotiation
behaviour (at least the parts that are most commonly used).
---
 servconf.c    | 27 +++++++++++++++++++++++++--
 session.c     | 25 +++++++++++++++++++++++--
 sshd_config   |  4 ++++
 sshd_config.4 | 13 ++++++++++++-
 4 files changed, 64 insertions(+), 5 deletions(-)

Index: openssh-7.4p1/servconf.c
===================================================================
--- openssh-7.4p1.orig/servconf.c	2017-04-04 17:13:00.514475472 +0300
+++ openssh-7.4p1/servconf.c	2017-04-04 17:13:00.502444348 +0300
@@ -150,7 +150,7 @@
 	options->client_alive_interval = -1;
 	options->client_alive_count_max = -1;
 	options->num_authkeys_files = 0;
-	options->num_accept_env = 0;
+	options->num_accept_env = -1;
 	options->permit_tun = -1;
 	options->num_permitted_opens = -1;
 	options->adm_forced_command = NULL;
@@ -390,6 +390,25 @@
 		options->max_sessions = DEFAULT_SESSIONS_MAX;
 	if (options->use_dns == -1)
 		options->use_dns = 0;
+	if (options->num_accept_env == -1) {
+		options->num_accept_env = 0;
+		options->accept_env[options->num_accept_env++] =
+		    xstrdup("LANG");
+		options->accept_env[options->num_accept_env++] =
+		    xstrdup("LC_ALL");
+		options->accept_env[options->num_accept_env++] =
+		    xstrdup("LC_CTYPE");
+		options->accept_env[options->num_accept_env++] =
+		    xstrdup("LC_COLLATE");
+		options->accept_env[options->num_accept_env++] =
+		    xstrdup("LC_TIME");
+		options->accept_env[options->num_accept_env++] =
+		    xstrdup("LC_NUMERIC");
+		options->accept_env[options->num_accept_env++] =
+		    xstrdup("LC_MONETARY");
+		options->accept_env[options->num_accept_env++] =
+		    xstrdup("LC_MESSAGES");
+	}
 	if (options->client_alive_interval == -1)
 		options->client_alive_interval = 0;
 	if (options->client_alive_count_max == -1)
@@ -1701,11 +1720,15 @@
 			if (strchr(arg, '=') != NULL)
 				fatal("%s line %d: Invalid environment name.",
 				    filename, linenum);
+			if (options->num_accept_env == -1)
+				options->num_accept_env = 0;
 			if (options->num_accept_env >= MAX_ACCEPT_ENV)
 				fatal("%s line %d: too many allow env.",
 				    filename, linenum);
 			if (!*activep)
 				continue;
+			if (strcmp(arg, "none") == 0)
+				continue;
 			options->accept_env[options->num_accept_env++] =
 			    xstrdup(arg);
 		}
@@ -2138,7 +2161,7 @@
 	} \
 } while(0)
 #define M_CP_STRARRAYOPT(n, num_n) do {\
-	if (src->num_n != 0) { \
+	if (src->num_n != 0 && src->num_n != -1) { \
 		for (dst->num_n = 0; dst->num_n < src->num_n; dst->num_n++) \
 			dst->n[dst->num_n] = xstrdup(src->n[dst->num_n]); \
 	} \
Index: openssh-7.4p1/session.c
===================================================================
--- openssh-7.4p1.orig/session.c	2017-04-04 17:13:00.514475472 +0300
+++ openssh-7.4p1/session.c	2017-04-04 17:13:00.503072816 +0300
@@ -860,6 +860,18 @@
 }
 
 /*
+ * If the given environment variable is set in the daemon's environment,
+ * push it into the new child as well. If it is unset, do nothing.
+ */
+static void
+child_inherit_env(char ***envp, u_int *envsizep, const char *name)
+{
+	char *value;
+	if ((value = getenv(name)) != NULL)
+		child_set_env(envp, envsizep, name, value);
+}
+
+/*
  * Reads environment variables from the given file and adds/overrides them
  * into the environment.  If the file does not exist, this does nothing.
  * Otherwise, it must consist of empty lines, comments (line starts with '#')
@@ -1022,6 +1034,16 @@
 	ssh_gssapi_do_child(&env, &envsize);
 #endif
 
+	/* Default to the system-wide locale/language settings. */
+	child_inherit_env(&env, &envsize, "LANG");
+	child_inherit_env(&env, &envsize, "LC_ALL");
+	child_inherit_env(&env, &envsize, "LC_CTYPE");
+	child_inherit_env(&env, &envsize, "LC_COLLATE");
+	child_inherit_env(&env, &envsize, "LC_TIME");
+	child_inherit_env(&env, &envsize, "LC_NUMERIC");
+	child_inherit_env(&env, &envsize, "LC_MONETARY");
+	child_inherit_env(&env, &envsize, "LC_MESSAGES");
+
 	/* Set basic environment. */
 	for (i = 0; i < s->num_env; i++)
 		child_set_env(&env, &envsize, s->env[i].name, s->env[i].val);
@@ -1062,8 +1084,7 @@
 	/* Normal systems set SHELL by default. */
 	child_set_env(&env, &envsize, "SHELL", shell);
 
-	if (getenv("TZ"))
-		child_set_env(&env, &envsize, "TZ", getenv("TZ"));
+	child_inherit_env(&env, &envsize, "TZ");
 
 #ifdef PER_SESSION_XAUTHFILE
         if (s->auth_file != NULL)
Index: openssh-7.4p1/sshd_config
===================================================================
--- openssh-7.4p1.orig/sshd_config	2017-04-04 17:13:00.514475472 +0300
+++ openssh-7.4p1/sshd_config	2017-04-04 17:13:00.503276771 +0300
@@ -26,6 +26,10 @@
 #SyslogFacility AUTH
 #LogLevel INFO
 
+# Use the client's locale/language settings
+#AcceptEnv LANG LC_ALL LC_CTYPE LC_COLLATE LC_TIME LC_NUMERIC
+#AcceptEnv LC_MONETARY LC_MESSAGES
+
 # Authentication:
 
 #LoginGraceTime 2m
