libhdate (1.6.02-2+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Wed, 18 Nov 2020 17:08:11 +0300

libhdate (1.6.02-2) unstable; urgency=medium

  * Update maintainer address (Closes: #899576)

 -- Lior Kaplan <kaplan@debian.org>  Mon, 30 Jul 2018 06:49:11 +0300

libhdate (1.6.02-1) unstable; urgency=medium

  * New upstream release:
    - manpage.patch dropped (merged upstream)
    - patch fix_3 dropped (merged upstream)
    - nested_extern.patch dropped (merged upstream)
    - missing_format.patch dropped (merged upstream)
  * Remove some Multiarch settings. A temporary workaround for now.
    (Closes: #807516, #818539)
  * misc:Pre-Depends' instead of multiarch-support
  * Build-Depend on dh-python
  * HTTPS VCS browser URL.
  * typos_output.patch: renamed typos.patch. More typos from Lintian.
  * A DEP-5 copyright file
  * Standard-Version 3.9.8

 -- Tzafrir Cohen <tzafrir@debian.org>  Tue, 24 Jan 2017 19:36:42 +0200

libhdate (1.6-3) unstable; urgency=medium

  * Incorporate previous NMUs.
  * Fix FTBFS with clang instead of GCC (Closes: #758453).
    - Thanks Alexander Ovchinnikov.
  * Thanking Baruch Even for his work on this package (Closes: #60005).

 -- Lior Kaplan <kaplan@debian.org>  Mon, 17 Aug 2015 17:18:17 +0200

libhdate (1.6-2.2) unstable; urgency=medium

  * Non-maintainer upload with maintainer's permission
  * Add dh-autoreconf to fix FTBFS on ppc64el. Closes: #757112, #757136
  * Add multi-arch. Closes: #757113, #727006

 -- Andreas Barth <aba@ayous.org>  Wed, 10 Sep 2014 15:03:37 +0000

libhdate (1.6-2.1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * Non-maintainer upload with maintainer's permission

  [ gregor herrmann ]
  * Fix "hardcodes /usr/lib/perl5"
    - use $Config{vendorarch} in debian/rules and debian/libhdate-perl.*
    - make the latter two executable
    (Closes: #752348)

 -- Damyan Ivanov <dmn@debian.org>  Tue, 29 Jul 2014 06:38:35 +0000

libhdate (1.6-2) unstable; urgency=low

  * Patch fix_3: fix an endless loop with hcal -3 (Closes: #692039).
  * Patch time_t: fixes a size issue on x32 (Closes: #719808).
  * Switch to dh.
  * Compat level 9.
  * autotools-dev for new config.{sub,guess} for e.g. arm64.
  * A symbols file for libhdate1.
  * typo_output.patch: fix a typo "ouput" (Lintian).
  * Bump standards version: 3.9.4.
  * cflags.patch: pass along build-flags set by deb-helpers.
  * As a result of building with warnings and hardening, several other
    fixes: 
    - missing_format.patch - missing a format string for fprintf.
    - duplicate_gnu_source.patch - _GNU_SOURCE was set both in autoconf
      and in files.
    - nested_extern.patch - Don't #include files inside a function.
    - size_t.patch - int vs. size_t issues.
  * feb.patch: calendars of February mat have more than 4 weeks (Closes:
    #696814).

 -- Tzafrir Cohen <tzafrir@debian.org>  Sat, 21 Sep 2013 14:17:40 +0300

libhdate (1.6-1) unstable; urgency=low

  * New upstream release
  * Drop libhdate-php package

 -- Lior Kaplan <kaplan@debian.org>  Sat, 07 Apr 2012 19:31:21 +0300

libhdate (1.4.20-4) unstable; urgency=low

  * Migrate from dh_pycentral to dh_python2 (Closes: #616863)

 -- Lior Kaplan <kaplan@debian.org>  Fri, 29 Jul 2011 15:45:24 +0200

libhdate (1.4.20-3) unstable; urgency=low

  * Drop *.la from -dev package per policy 10.2 (Closes: #621670)

 -- Lior Kaplan <kaplan@debian.org>  Fri, 08 Apr 2011 01:53:14 +0300

libhdate (1.4.20-2) unstable; urgency=low

  * Fix build failure with ld --as-needed (Closes: #604780)
    - Apply patch from Matthias Klose (Ubuntu).

 -- Lior Kaplan <kaplan@debian.org>  Tue, 05 Apr 2011 22:50:16 +0300

libhdate (1.4.20-1) unstable; urgency=low

  * New upstream release
    - Inlcudes man page patch (removing it from debian/patches)

 -- Lior Kaplan <kaplan@debian.org>  Sat, 13 Nov 2010 23:46:24 +0200

libhdate (1.4.19-2) unstable; urgency=low

  * Rename python bindings to python-hdate (Thanks to Luca Falavigna).

 -- Lior Kaplan <kaplan@debian.org>  Wed, 03 Nov 2010 23:34:02 +0200

libhdate (1.4.19-1) unstable; urgency=low

  * New upstream release
    - Returns null if no parasha is returned (Closes: #583092)
  * Switch to dpkg-source 3.0 (quilt) format (Closes: #583235)
  * debian/control:
    - Rename libhdate-python to python-libhdate (Closes: #497774)
    - Provide PHP bindings with php-libhdate.

 -- Lior Kaplan <kaplan@debian.org>  Sun, 31 Oct 2010 23:44:16 +0200

libhdate (1.4.12-2) unstable; urgency=low

  * Remove libhdate-pascal package as fpc was removed from Lenny

 -- Baruch Even <baruch@debian.org>  Mon, 01 Dec 2008 14:05:11 +0200

libhdate (1.4.12-1) unstable; urgency=low

  * New upstream release
    - Pascal binding can be compiled with gpc (Closes: #486095)
  * Build python binding with python2.5 instead of python2.4.

 -- Lior Kaplan <kaplan@debian.org>  Thu, 04 Sep 2008 11:35:29 +0300

libhdate (1.4.11-1) unstable; urgency=low

  * New upstream release (Closes: #462781)
  * Add ${shlibs:Depends} to arch:any packages in order to fix 
    missing-dependency-on-libc lintian error.

 -- Lior Kaplan <kaplan@debian.org>  Fri, 28 Mar 2008 11:09:19 +0300

libhdate (1.4.10-1) unstable; urgency=low

  * New upstream release
    - Upstream changed license to GPL3 from GPL2

 -- Baruch Even <baruch@debian.org>  Sat, 05 Jan 2008 22:30:38 +0200

libhdate (1.4.9-3) unstable; urgency=low

  * Add arm as a supported arch for libhdate-pascal (Closes: #457448)
  * Increased Standards-Version to 3.7.3, No changes needed.
  * Moved the Homepage to the control headers from the description
  * Do not disable errors in make distclean
  * Converted XS-Vcs-* to Vcs-*
  * Add section description to appease the manpage gods
  * Fix libhdate-pascal to actually include the built files

 -- Baruch Even <baruch@debian.org>  Fri, 28 Dec 2007 18:37:14 +0200

libhdate (1.4.9-2) unstable; urgency=low

  * Update debian/control so packages are binNMU safe.
    See http://wiki.debian.org/binNMU for more info.

 -- Lior Kaplan <kaplan@debian.org>  Thu, 21 Jun 2007 20:29:23 +0100

libhdate (1.4.9-1) unstable; urgency=low

  * New upstream release
    - Fixes getopt variable usage (Closes: #414206)
  * Add XS-Vcs-Svn and XS-Vcs-Browser to the control file
  * Update dep on python-central to >= 0.5 as per lintian

 -- Baruch Even <baruch@debian.org>  Sun,  8 Apr 2007 12:15:46 +0300

libhdate (1.4.8-1) unstable; urgency=low

  * New upstream release
  * Remove hardcoded python version (2.3), as preparation for the Python
    transition.
  * Comply with the new Python policy.

 -- Lior Kaplan <kaplan@debian.org>  Sat,  8 Jul 2006 23:04:56 +0300

libhdate (1.4.6-1) unstable; urgency=low

  * New upstream release
  * debian/control: upgrade Standards-Version to 3.7.2 (no changes needed)

 -- Lior Kaplan <kaplan@debian.org>  Mon, 15 May 2006 19:05:45 -0500

libhdate (1.4.3-2) unstable; urgency=low

  * Remove hardcoded i386 from libhdate-pascal. This enables non-empty packages 
    for other architectures.

 -- Lior Kaplan <kaplan@debian.org>  Mon, 15 May 2006 00:37:57 +0000

libhdate (1.4.3-1) unstable; urgency=low

  * New upstream release
  * debian/control: enable libhdate-pascal for AMD64
  * debian/watch: use uscan's sf.net shortcut

 -- Lior Kaplan <kaplan@debian.org>  Fri, 14 Apr 2006 22:30:14 +0300

libhdate (1.4.2-2) unstable; urgency=low

  * Update watch file

 -- Baruch Even <baruch@debian.org>  Sun, 29 Jan 2006 23:25:54 +0000

libhdate (1.4.2-1) unstable; urgency=low

  [ Baruch Even ]
  * Conflicts/Replaces with libhdate0 to smooth upgrades from old packages.
  * New upstream version

  [ Lior Kaplan ]
  * Fix FSF address in debian/copyright

 -- Baruch Even <baruch@debian.org>  Thu,  1 Dec 2005 11:28:16 +0000

libhdate (1.3.0-3) unstable; urgency=low

  * debian/control:
     - Change section to match overrides

 -- Baruch Even <baruch@debian.org>  Thu, 11 Aug 2005 15:52:35 +0100

libhdate (1.3.0-2) unstable; urgency=low

  * debian/control: 
     - Limit the build dependency of fp-compiler to i386, sparc & powerpc.
     - Build libhdate-pascl only on i386, sparc & powerpc
     - Add -s option (short for --same-arch) to all dh_* lines.

 -- Lior Kaplan <webmaster@guides.co.il>  Wed, 10 Aug 2005 23:10:05 +0300

libhdate (1.3.0-1) unstable; urgency=low

  * Initial release. (Closes: #291581: ITP: libhdate -- small C, C++
    library for Hebrew dates and holidays)
  * Based on package by Kobi Zamir <kzamir@wall.co.il>

 -- Lior Kaplan <webmaster@guides.co.il>  Wed,  3 Aug 2005 00:38:20 +0300
