#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

include /usr/share/dpkg/architecture.mk

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
export DEB_HOST_MULTIARCH

ifeq ($(origin CC),default)
CC := $(DEB_HOST_GNU_TYPE)-gcc
endif
export CC := gcc
export config_BUILD_CC:=$(CC)
export config_TARGET_CC:=$(CC)
export PATH:=$(PATH):$(CURDIR)/bin
export LDFLAGS:=$(LDFLAGS) -L$(CURDIR)/.libs

CONFIGURE_EXTRA_FLAGS = config_TARGET_TCL_INC="-I/usr/include/tcl8.6" \
	config_BUILD_CFLAGS="$(CFLAGS) -DTHREADSAFE=1" \
	config_TARGET_LIBS="-ltcl8.6 -lpthread" \
	--libdir=/usr/lib/$(DEB_HOST_MULTIARCH) \
	--enable-utf8

override_dh_auto_configure:
	mkdir -p $(CURDIR)/bin/
	ln -s $(shell which tclsh8.6) $(CURDIR)/bin/tclsh
		dh_auto_configure -- $(CONFIGURE_EXTRA_FLAGS)

override_dh_auto_build:
	$(MAKE) all libtclsqlite.la doc

override_dh_auto_install:
	install -d $(CURDIR)/debian/tmp/usr/bin/ \
		$(CURDIR)/debian/tmp/usr/lib/sqlite/
	install -m 0664 $(CURDIR)/debian/pkgIndex.tcl \
		$(CURDIR)/debian/tmp/usr/lib/sqlite
	./libtool --mode=install install libtclsqlite.la \
		$(CURDIR)/debian/tmp/usr/lib/sqlite
	install -d $(CURDIR)/debian/tmp/usr/share/lemon
	install -m 664 $(CURDIR)/tool/lempar.c \
		$(CURDIR)/debian/tmp/usr/share/lemon
	install -m 775 lemon $(CURDIR)/debian/tmp/usr/bin/
	dh_auto_install

override_dh_makeshlibs:
	# empty dependency_libs
	sed -i "/dependency_libs/ s/'.*'/''/" \
		`find $(CURDIR)/debian/ -name '*.la'`
	dh_makeshlibs -V -X"libtclsqlite.so*"

override_dh_auto_test:
	LDFLAGS="$(LDFLAGS) -ltcl8.6 -lpthread" dh_auto_test || true

%:
	dh $@

.PHONY: override_dh_auto_configure override_dh_auto_build \
	override_dh_auto_install override_dh_makeshlibs \
	override_dh_auto_test
