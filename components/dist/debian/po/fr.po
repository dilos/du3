# translation of fr.po to French
#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
# Christian Perrier <bubulle@debian.org>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: dist\n"
"Report-Msgid-Bugs-To: dist@packages.debian.org\n"
"POT-Creation-Date: 2012-02-20 07:13+0100\n"
"PO-Revision-Date: 2004-04-16 13:46+0100\n"
"Last-Translator: Christian Perrier <bubulle@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Name of your organization:"
msgstr "Nom de votre organisation�:"

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"You don't seem to have an /etc/news/organization file. Usually that contains "
"the name of your organization as you want it to appear on the Organization "
"line of outgoing articles/mail/patches. Please supply the name of your "
"organization as you want it to appear on the Organization line of outgoing "
"articles/patches.  (It is nice if this also specifies your location.  Your "
"city name is probably sufficient if well known.) For example:"
msgstr ""
"Vous n'avez pas de fichier /etc/news/organization. Habituellement, ce "
"fichier contient le nom de votre organisation, qui est inscrit sur la ligne "
"��Organization�� des articles, courriels et correctifs envoy�s. Veuillez "
"indiquer le nom de l'organisation que vous souhaitez voir figurer sur cette "
"ligne. Vous pouvez aussi indiquer votre adresse, ou simplement votre ville "
"si elle est connue. Exemple�:"

#. Type: string
#. Description
#: ../templates:1001
msgid "   University of Southern North Dakota, Hoople"
msgstr "   Universit� Pierre et Marie Curie, Paris"

#. Type: string
#. Description
#: ../templates:1001
msgid "Type in \"--none--\" if you do not want to specify one."
msgstr ""
"Si vous ne d�sirez pas sp�cifier d'organisation, inscrivez ��--none--��."
