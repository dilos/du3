#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

SHLIBVER := 0.4.14
libproxy := $(shell sed -nr 's/^Package:[[:space:]]*(libproxy[0-9]+(v5)?)[[:space:]]*$$/\1/p' debian/control.in)

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
#export DEB_LDFLAGS_MAINT_APPEND = -Wl,-O1 -Wl,-z,defs -Wl,--as-needed
export DEB_LDFLAGS_MAINT_APPEND = -Wl,-z,defs
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

export CC=gcc
export CXX=g++

DO_PACKAGES = $(shell dh_listpackages)

DH_ADDONS=--with=gnome
# only invoke dh --with cli if cli-common-dev is present
WITH_DOTNET = -DWITH_DOTNET=OFF
ifneq (,$(wildcard /usr/share/perl5/Debian/Debhelper/Sequence/cli.pm))
ifeq (,$(filter stage1,$(DEB_BUILD_PROFILES)))
DH_ADDONS += --with cli
WITH_DOTNET = -DWITH_DOTNET=ON
endif
endif

ifneq ($(filter amd64 arm64 armel armhf i386 mips mips64el mipsel ppc64el alpha hppa m68k powerpc powerpcspe sh4, $(DEB_HOST_ARCH)),)
WITH_MOZJS = -DWITH_MOZJS=ON
else
WITH_MOZJS = -DWITH_MOZJS=OFF
endif

CONFIGURE_FLAGS = $(WITH_DOTNET) \
		-DWITH_VALA=ON \
		-DGMCS_EXECUTABLE=/usr/bin/mono-csc \
		-DCMAKE_SKIP_RPATH=ON \
		-DBIPR=0 \
		-DLIB_INSTALL_DIR=/usr/lib/$(DEB_HOST_MULTIARCH) \
		-DLIBEXEC_INSTALL_DIR=/usr/lib/$(DEB_HOST_MULTIARCH)/libproxy/$(DEB_VERSION_UPSTREAM)

ifeq (,$(filter stage1,$(DEB_BUILD_PROFILES)))
CONFIGURE_FLAGS += \
		-DWITH_GNOME3=ON \
		$(WITH_MOZJS) \
		-DWITH_WEBKIT3=ON
else
CONFIGURE_FLAGS += \
		-DWITH_GNOME3=OFF \
		-DWITH_MOZJS=OFF \
		-DWITH_WEBKIT3=OFF
endif

ifeq (,$(filter python-libproxy,$(DO_PACKAGES)))
CONFIGURE_FLAGS += -DWITH_PYTHON2=OFF
endif
ifeq (,$(filter python3-libproxy,$(DO_PACKAGES)))
CONFIGURE_FLAGS += -DWITH_PYTHON3=OFF
endif

override_dh_auto_configure:
	dh_auto_configure -- $(CONFIGURE_FLAGS)

override_dh_install-arch:
	dh_install
	rm -f \
	  debian/libproxy-dev/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig/libproxy-sharp-*.pc

override_dh_install-indep:
	cp -a debian/tmp/usr/lib/$(shell pyversions -d) debian/tmp/usr/lib/python3
	dh_install
# FIXME: DilOS mono
#ifeq (,$(filter stage1,$(DEB_BUILD_PROFILES)))
#	sed -i 's!/usr/lib/mono/libproxy-sharp!/usr/lib/cli/libproxy-sharp-0.4!' \
#		debian/libproxy-cil-dev/usr/lib/pkgconfig/libproxy-sharp-1.0.pc
#endif

override_dh_makeshlibs:
	dh_makeshlibs -V '$(libproxy) (>= $(SHLIBVER))' -- -c4

override_dh_gencontrol:
	dh_gencontrol -- -Vlibproxy=${libproxy}

%:
	dh $@ $(DH_ADDONS)
build binary %-indep: DH_ADDONS+=--with=python2,python3
