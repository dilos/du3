// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = require("!!NODE_PATH/css-loader/index.js!NODE_PATH/postcss-loader/index.js!CSS_INPUT");
if(typeof content === 'string') content = [[module.id, content, '']];
// add the styles to the DOM
var update = require("!NODE_PATH/style-loader/addStyles.js")(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(module.hot) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!NODE_PATH/css-loader/index.js!NODE_PATH/postcss-loader/index.js!CSS_INPUT", function() {
			var newContent = require("!!NODE_PATH/css-loader/index.js!NODE_PATH/postcss-loader/index.js!CSS_INPUT");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}
