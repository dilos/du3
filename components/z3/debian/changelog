z3 (4.4.1-1~deb10u1+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Mon, 09 Aug 2021 18:38:54 +0300

z3 (4.4.1-1~deb10u1) buster; urgency=medium

  * Non-maintainer upload.
  * Rebuild for buster.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 24 Aug 2019 12:18:35 +0200

z3 (4.4.1-1) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Team Upload
  * Upload to unstable

  [ Andreas Beckmann ]
  * Do not set the SONAME of libz3java.so to libz3.so.4.  (Closes: #842892)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 17 Aug 2019 11:05:53 +0200

z3 (4.4.1-0.5~exp1) experimental; urgency=medium

  * Package moved to salsa (Closes: #926939)
  * Standards-Version updated to 4.2.1
  * Fix priority-extra-is-replaced-by-priority-optional warning
  * Moved under the llvm umbrella

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 16 Apr 2019 14:30:31 +0200

z3 (4.4.1-0.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Remove the incorrect Multi-Arch: same of python-z3,
    thanks to Helmut Grohne. (Closes: #874237)

 -- Adrian Bunk <bunk@debian.org>  Sun, 09 Sep 2018 22:28:32 +0300

z3 (4.4.1-0.3) unstable; urgency=medium

  * Non-maintainer upload.

  [ Fabian Wolff ]
  * debian/patches/f02d273ee39ae047222e362c37213d29135dc661.patch:
    Fix build failure with new gnu++14 standard. (Closes: #835754)
  * debian/patches/27399309009314f56cdfbd8333f287b1a9b7a3a6.patch:
    Fix build failure with new compiler and clang. (Closes: #835743)

  [ Gianfranco Costamagna ]
  * debian/patches/fix-build.patch: tweak the casts a little bit
    more to really fix 835743.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sun, 25 Sep 2016 23:06:24 +0200

z3 (4.4.1-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Add patch kfreebsd.patch to attempt to fix a FTBFS on kfreebsd-*.
  * Adjust avoid-ocamlopt.patch to remove all calls to ocamlopt.
  * Build libz3-cil only where Mono is available.
  * Build libz3-jni and libz3-java only where Java is available.
  * Remove enable-dotnet.patch and recreate its effect with sed in
    debian/rules depending on whether Mono is available or not.
  * Disable some tests that might fail on some platforms.
    Forwarded: https://github.com/Z3Prover/z3/issues/687
  * Enable hardening flags for libz3-ocaml-dev.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Tue, 19 Jul 2016 23:02:19 +0200

z3 (4.4.1-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release.
  * Remove patches that were fixed upstream:
     - signed_char01
     - signed_char02
     - signed_char03
     - signed_char04
     - disable_test
     - disable_test2
     - disable_test3
     - disable_test4 (upstream issue has been closed:
        https://github.com/Z3Prover/z3/issues/210)
     - fix_conflict
  * Migrate remaining patches to DEP-3 format.
  * Rewrite typos.patch to apply to new version of z3.
  * Add patch fix-dotnet-version.patch to fix a FTBFS with Mono caused
    by an apparently unsupported version of the .NET framework
    (Closes: #808695).
  * Add patch disable-tests.patch to disable three potentially failing
    tests in src/test/main.cpp.
  * Install shared libraries into new libz3-4 package (Closes: #819884).
  * Remove ${shlibs:Depends} and ${misc:Pre-Depends} substitution
    variables from libz3-dev entry in debian/control.
  * Install python files directly into /usr/lib/python2.7/dist-packages/
    (Closes: #802272).
  * Clean up debian/rules.
  * Remove unnecessary version restriction of build dependency
    cli-common-dev.
  * Fix debian/copyright: Change MIT to Expat and delete file block that
    no longer applies (copyright of hamiltonian.py changed).
  * Move libz3-ocaml-dev into section ocaml.
  * Add preinst scripts to remove directories from older versions to allow
    debhelper to install symlinks (Closes: #823573).
  * Enable hardening flags in debian/rules.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Wed, 13 Jul 2016 17:33:19 +0200

z3 (4.4.0-5) unstable; urgency=low

  * Add fixes for tests previously failing on ARM, PowerPC

 -- Michael Tautschnig <mt@debian.org>  Sun, 04 Oct 2015 10:57:27 +0100

z3 (4.4.0-4) unstable; urgency=low

  * Add libz3-jni package to split out arch-specific files from java package.
    (Closes: #797515)
  * Don't claim multi-arch compatibilty of libz3-cil. (Closes: #797514)
  * ocamlopt isn't universally available, and never actually used in the build
    process.

 -- Michael Tautschnig <mt@debian.org>  Fri, 04 Sep 2015 13:55:20 +0100

z3 (4.4.0-3) unstable; urgency=low

  * De-duplicate files in python-z3, depend on libz3-dev instead
  * Added bindings for java, OCaml, and .NET
  * Use multi-arch library locations

 -- Michael Tautschnig <mt@debian.org>  Thu, 27 Aug 2015 15:18:46 +0100

z3 (4.4.0-2) unstable; urgency=low

  * Added __init__.py. Thanks Andrea Villa for the hints. (Closes: #791604)
  * Use emmintrin.h conditionally only (Closes: #789881)

 -- Michael Tautschnig <mt@debian.org>  Tue, 07 Jul 2015 08:08:48 +0100

z3 (4.4.0-1) unstable; urgency=low

  * Initial release (Closes: #786807)
  * Includes patches from unstable branch up to 6a50f10b8b

 -- Michael Tautschnig <mt@debian.org>  Wed, 17 Jun 2015 21:40:44 +0100
