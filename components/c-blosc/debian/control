Source: c-blosc
Priority: optional
Section: libs
Maintainer: Daniel Stender <stender@debian.org>
Build-Depends: debhelper (>= 11),
               cmake,
               liblz4-dev (>= 0.0~r130),
               libsnappy-dev,
               zlib1g-dev,
               libzstd-dev,
               python-docutils <!nodoc>,
               links <!nodoc>,
               pandoc [fixme] <!nodoc>
Standards-Version: 4.2.1
Homepage: http://blosc.org/
Vcs-Browser: https://salsa.debian.org/debian/c-blosc
Vcs-Git: https://salsa.debian.org/debian/c-blosc.git

Package: libblosc-dev
Section: libdevel
Architecture: any
Depends: libblosc1 (= ${binary:Version}),
         ${misc:Depends}
Description: high performance meta-compressor optimized for binary data (development files)
 Blosc is a high performance compressor optimized for binary data. It has been
 designed to transmit data to the processor cache faster than the traditional,
 non-compressed, direct memory fetch approach via a memcpy() OS call. Blosc is
 meant not only to reduce the size of large datasets on-disk or in-memory, but
 also to accelerate memory-bound computations.
 .
 It uses the blocking technique to reduce activity on the memory bus as much
 as possible. In short, this technique works by dividing datasets in blocks
 that are small enough to fit in caches of modern processors and perform
 compression / decompression there. It also leverages, if available, SIMD
 instructions (SSE2) and multi-threading capabilities of CPUs, in order to
 accelerate the compression / decompression process to a maximum.
 .
 This package contains the development files required to build programs against
 Blosc.

Package: libblosc1
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: high performance meta-compressor optimized for binary data
 Blosc is a high performance compressor optimized for binary data. It has been
 designed to transmit data to the processor cache faster than the traditional,
 non-compressed, direct memory fetch approach via a memcpy() OS call. Blosc is
 meant not only to reduce the size of large datasets on-disk or in-memory, but
 also to accelerate memory-bound computations.
 .
 It uses the blocking technique to reduce activity on the memory bus as much
 as possible. In short, this technique works by dividing datasets in blocks
 that are small enough to fit in caches of modern processors and perform
 compression / decompression there. It also leverages, if available, SIMD
 instructions (SSE2) and multi-threading capabilities of CPUs, in order to
 accelerate the compression / decompression process to a maximum.
