python-pyproj (1.9.6-1+dilos1) unstable; urgency=medium

  * Build for DilOS.

 -- DilOS Team <dilos@dilos.org>  Mon, 06 Jul 2020 11:10:17 +0300

python-pyproj (1.9.6-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.3.0, no changes.
  * Drop autopkgtests to test installability & module import.
  * Add lintian override for testsuite-autopkgtest-missing.
  * Update watch file to limit matches to archive path.
  * Update copyright file for PROJ changes.
  * Drop Cython patch, included upstream. Refresh remaining patches.
  * Use nose2 for tests.
  * Add patch to not cythonize in the clean target.
  * Don't move _proj.c out of the way, file no longer included upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 30 Dec 2018 08:59:54 +0100

python-pyproj (1.9.5.1-4) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Update copyright-format URL to use HTTPS.
  * Bump Standards-Version to 4.1.4, no changes.
  * Check DEB_BUILD_PROFILES for nocheck in dh_auto_test override.
  * Rename PROJ.4 to PROJ.
  * Update Vcs-* URLs for Salsa.
  * Add module import tests to autopkgtest configuration.
  * Drop ancient X-Python{,3}-Version fields.
  * Strip trailing whitespace from control & rules files.
  * Use DEP-3 headers for Cython patch, specifically Origin header.
  * Move _proj.c out of the way before the build and restore it afterwards.

  [ Iain Lane ]
  * debian/patches/Use-Cython-in-setup.py-to-generate-C-code-on-the-fly.patch:
    Cherry-pick from upstream. This makes _proj.c get regenerated during the
    build, which fixes FTBFS when it is incompatible with the current python
    version (e.g. the shipped _proj.c doesn't work with python 3.7).
  * debian/control: Build-Depend on cython for the above.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 04 Jul 2018 16:09:05 +0200

python-pyproj (1.9.5.1-3) unstable; urgency=medium

  * Add patch to fix 'international' typo.
  * Bump Standards-Version to 4.0.0, no changes.
  * Add autopkgtest to test installability.
  * Enable PIE hardening buildflags.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 26 Jun 2017 07:48:46 +0200

python-pyproj (1.9.5.1-2) unstable; urgency=medium

  * Update Vcs-Git URL to use HTTPS.
  * Update dh_python calls to act on specific package.
  * Bump Standards-Version to 3.9.8, no changes.
  * Drop dh_numpy{,3} calls, numpy support disabled upstream.
    (closes: #821109)
  * Enable all hardening buildflags, except PIE (causes build failure).

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 15 Apr 2016 18:25:26 +0200

python-pyproj (1.9.5.1-1) unstable; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 06 Jan 2016 23:53:14 +0100

python-pyproj (1.9.5-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright file, changes:
    - Update copyright years for Charles Karney
    - Add Even Rouault & Andrey Kiselev to copyright holders
    - Add license & copyright for src/PJ_sch.c
  * Drop 02-use_setup-proj.patch, no longer required.
    setup.py now supports system proj directly.
  * Update Vcs-Browser URL to use HTTPS.
  * Update watch file, handle alternative tar extensions.
  * Add patch to have setup.py not modify lib/pyproj/datadir.py*.
    setup.py doesn't need to modify lib/pyproj/datadir.py,
    it is already patched for use by the Debian package.
  * Enable unit tests with pybuild override.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 06 Jan 2016 23:41:02 +0100

python-pyproj (1.9.4-1) unstable; urgency=medium

  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 03 May 2015 22:16:14 +0200

python-pyproj (1.9.4-1~exp2) experimental; urgency=medium

  * Require at least libproj-dev 4.9.0~ for geodesic.h.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 01 Mar 2015 11:38:49 +0100

python-pyproj (1.9.4-1~exp1) experimental; urgency=medium

  * New upstream release.
  * Add myself to Uploaders.
  * Update watch file with sepwatch changes.
  * Add gbp.conf to use pristine-tar by default.
  * Restructure control file with cme.
  * Bump debhelper compatibility to 9.
  * Use canonical Vcs-* URLs.
  * Enable parallel builds.
  * Additional cleanup of generated files.
  * Set pybuild name to pyproj.
  * Remove pyversion file and add X-Python*-Version to control file.
  * Drop doc-base registration for README.html, no longer included.
  * Update copyright file using copyright-format 1.0.
  * Use setup-proj.py for system installed Proj.4 library.
  * Update watch file for GitHub releases.
  * Add upstream metadata.
  * Update Homepage URL for move to GitHub.
  * Bump Standards-Version to 3.9.6, changes: canonical Vcs-* URLs,
    copyright-format 1.0.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 01 Mar 2015 04:29:13 +0100

python-pyproj (1.8.9-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add support for python3 (Closes: #720379)
    - Add python3-pyroj to debian/control
    - Add dh-python and python3-all-dev to build depends
    - Build with python3 and buildstysem=pybuild
    - Add debian/python-pyproj.install debian/python3-pyproj.install for
      multi-binary package
  * Drop obsolete build-dep on pysupport

 -- Scott Kitterman <scott@kitterman.com>  Wed, 21 Aug 2013 01:26:12 -0400

python-pyproj (1.8.9-1) unstable; urgency=low

  * New upstream version
  * Patches refreshed
  * Use dh_python2 in debian/rules

 -- David Paleino <dapal@debian.org>  Wed, 09 Mar 2011 17:39:24 +0100

python-pyproj (1.8.8-3) unstable; urgency=low

  * Upload to unstable.

 -- David Paleino <dapal@debian.org>  Sat, 12 Feb 2011 17:18:00 +0100

python-pyproj (1.8.8-2) experimental; urgency=low

  * Upload to ease sync to Ubuntu
  * debian/changelog: merged Ubuntu's "pyproj" changelog

 -- David Paleino <dapal@debian.org>  Mon, 06 Dec 2010 20:20:31 +0100

python-pyproj (1.8.8-1) experimental; urgency=low

  * New upstream version
  * debian/watch: don't use the redirector anymore, since I'm going
    to shut it down
  * debian/patches/02-dont_compile_datums.patch added: don't compile
    datum files, since we're using the ones provided by proj-data

 -- David Paleino <dapal@debian.org>  Thu, 16 Sep 2010 11:25:55 +0200

python-pyproj (1.8.7-1) unstable; urgency=low

  * New upstream version
  * debian/control:
    - Standards-Version bumped to 3.9.1, no changes needed

 -- David Paleino <dapal@debian.org>  Mon, 02 Aug 2010 21:39:58 +0200

python-pyproj (1.8.6-3) unstable; urgency=low

  * debian/control:
    - long description improved.
    - Standards-Version bumped to 3.8.4, no changes needed
    - added Vcs-* fields
    - Build-Depend on python-all-dev, instead of python-dev
  * debian/watch fixed to use googlecode.debian.net redirector
    (Closes: #582869)
  * debian/pyversions added (instead of XS-Python-Version in
    debian/control)
  * debian/README.source removed

 -- David Paleino <dapal@debian.org>  Tue, 15 Jun 2010 18:06:00 +0200

python-pyproj (1.8.6-2) unstable; urgency=low

  * debian/control, debian/rules: remove usage of quilt
    (Closes: #561395)

 -- David Paleino <dapal@debian.org>  Fri, 18 Dec 2009 23:31:53 +0100

python-pyproj (1.8.6-1) unstable; urgency=low

  * Initial release. (Closes: #515053)

 -- David Paleino <dapal@debian.org>  Fri, 11 Dec 2009 19:38:28 +0100

pyproj (1.8.6-0ubuntu1) lucid; urgency=low

  * New upstream release.
  * debian/copyright: Update to the new format template.
  * debian/control: Bump Standards.
  * Add debian/README.source to make lintian happy.

 -- Alessio Treglia <quadrispro@ubuntu.com>  Mon, 09 Nov 2009 11:22:49 +0100

pyproj (1.8.5-0ubuntu4) lucid; urgency=low

  * Limit override_dh_fixperms to architecture independent packages to fix
    FTBFS on non-i386 arch (LP: #462619)

 -- Scott Kitterman <scott@kitterman.com>  Wed, 28 Oct 2009 09:50:37 -0400

pyproj (1.8.5-0ubuntu3) karmic; urgency=low

  * Rework package to build again (FTBFS in Karmic rebuild test):
    - Convert to debhelper minimal rules
    - Update version requirements for debhelper and quilt
    - Drop cdbs build-depends
    - Bump compat to 7

 -- Scott Kitterman <scott@kitterman.com>  Mon, 26 Oct 2009 23:54:00 -0400

pyproj (1.8.5-0ubuntu2) jaunty; urgency=low

  * No change rebuild with python2.6.

 -- Matthias Klose <doko@ubuntu.com>  Thu, 26 Feb 2009 13:35:56 +0000

pyproj (1.8.5-0ubuntu1) jaunty; urgency=low

  * Initial Release (LP: #326996).

 -- Chris Murphy <chrismurf+ubuntu@gmail.com>  Sun, 08 Feb 2009 23:41:29 -0500
